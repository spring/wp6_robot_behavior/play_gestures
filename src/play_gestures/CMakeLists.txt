cmake_minimum_required(VERSION 3.0.2)
project(play_gestures)

# required packages
find_package(catkin REQUIRED COMPONENTS
    rostest
    rospy
    sensor_msgs
    std_msgs
)

# Install the python package group_navigation
catkin_python_setup()

catkin_package(
    CATKIN_DEPENDS
      rospy
      sensor_msgs
      std_msgs
)

# install nodes and scripts
catkin_install_python(
  PROGRAMS nodes/play_arm_recorded_gestures_main.py
           nodes/play_arm_gestures_action_client_main.py
           nodes/play_arm_gestures_action_server_main.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

# add tests
# add_rostest(test/test_group_detector_node.test)
