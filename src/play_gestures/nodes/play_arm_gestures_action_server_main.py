#!/usr/bin/env python
import rospy
from play_gestures import PlayArmGesturesActionServer


if __name__ == '__main__':
    action_name = 'play_arm_gestures_action'
    server = PlayArmGesturesActionServer(action_name=action_name)
    rospy.spin()
