#!/usr/bin/env python
import rospy
from play_gestures import PlayArmGesturesRosNode


if __name__ == '__main__':
    node_name = 'play_arm_gestures'    
    play_arm_gestures_ros_node = PlayArmGesturesRosNode(
            node_name=node_name)
    rospy.spin()
