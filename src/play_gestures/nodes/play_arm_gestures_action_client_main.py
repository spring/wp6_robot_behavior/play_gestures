#!/usr/bin/env python
import rospy
from play_gestures import PlayArmGesturesActionClient


if __name__ == '__main__':
    action_name = 'play_arm_gestures_action'
    client = PlayArmGesturesActionClient(action_name=action_name)
    result = client.call_server()
    rospy.loginfo('Result is : {}'.format(result))
