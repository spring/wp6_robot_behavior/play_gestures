#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import os
import re
from play_gestures.play_arm_gestures import PlayArmGestures
from utils import convert_dir_vec_to_pose
from numpy.testing import assert_array_almost_equal


# mean_dir_vec = np.array([ 0.0154009, -0.9690125, -0.0884354, -0.0022264, -0.8655276, 0.4342174, -0.0035145, -0.8755367, -0.4121039, -0.9236511, 0.3061306, -0.0012415, -0.5155854,  0.8129665,  0.0871897, 0.2348464,  0.1846561,  0.8091402,  0.9271948,  0.2960011, -0.013189 ,  0.5233978,  0.8092403,  0.0725451, -0.2037076, 0.1924306,  0.8196916])
# mean_dir_vec = mean_dir_vec.reshape(-1, 3)
# mean_dir_vec_n = np.linalg.norm(mean_dir_vec, axis=1)
# print(mean_dir_vec.shape, mean_dir_vec_n.shape)
# print(mean_dir_vec_n)

def test_compute_dir_vec_from_angle_pos(display=False, plot_from_dir_vec=False, save=False, dataset='ted'):
    play_arm_gestures = PlayArmGestures(dataset=dataset)
    joints_data = np.zeros((9, 8))
    dir_vecs = np.zeros((1, joints_data.shape[0], play_arm_gestures.n_joints + 1, 3))
    poses = np.zeros((1, joints_data.shape[0], play_arm_gestures.n_joints + 2, 3))
    
    # pose 1 test

    # pose 2 test  
    joints_data[1, 6] = -np.pi/4
    joints_data[1, 7] = np.pi/2
    joints_data[1, 2] = -np.pi/4
    joints_data[1, 3] = np.pi/2

    # pose 3 test
    joints_data[2, 2] = np.pi/4
    joints_data[2, 3] = np.pi/2
    joints_data[2, 6] = np.pi/4
    joints_data[2, 7] = np.pi/2

    # pose 4 test
    joints_data[3, 0] = np.pi/4
    joints_data[3, 1] = np.pi/4
    joints_data[3, 4] = np.pi/4
    joints_data[3, 5] = np.pi/4

    # pose 5 test
    joints_data[4, 0] = np.pi/4
    joints_data[4, 1] = np.pi/4
    joints_data[4, 2] = np.pi/4
    joints_data[4, 4] = np.pi/4
    joints_data[4, 5] = np.pi/4
    joints_data[4, 6] = np.pi/4

    # pose 6 test
    joints_data[5, 0] = np.pi/4
    joints_data[5, 1] = np.pi/6
    joints_data[5, 2] = -np.pi/4
    joints_data[5, 4] = np.pi/4
    joints_data[5, 5] = np.pi/6
    joints_data[5, 6] = np.pi/4

    # pose 7 test
    joints_data[6, 0] = np.pi/4
    joints_data[6, 1] = np.pi/6
    joints_data[6, 2] = -np.pi/4
    joints_data[6, 3] = np.pi/2

    joints_data[6, 4] = np.pi/4
    joints_data[6, 5] = np.pi/6
    joints_data[6, 6] = np.pi/4
    joints_data[6, 7] = np.pi/2

    # pose 8 test
    joints_data[7, 0] = -np.pi/6
    joints_data[7, 1] = np.pi/4
    joints_data[7, 2] = -np.pi/4
    joints_data[7, 3] = np.pi/2

    joints_data[7, 4] = np.pi/6
    joints_data[7, 5] = -np.pi/6
    joints_data[7, 6] = -np.pi/4
    joints_data[7, 7] = np.pi/3

    # pose 9 test
    joints_data[8, 0] = np.pi*2/3
    joints_data[8, 1] = np.pi/4
    joints_data[8, 2] = -np.pi/4
    joints_data[8, 3] = np.pi/3

    joints_data[8, 4] = np.pi/2
    joints_data[8, 5] = np.pi*2/3
    joints_data[8, 6] = -np.pi/4
    joints_data[8, 7] = np.pi/3


    for i, joints in enumerate(joints_data):
        dir_vecs[0, i, :, :] = np.array(play_arm_gestures.compute_dir_vec_from_angle_pos(joints=joints))
        poses[0, i, :, :] = convert_dir_vec_to_pose(vec=dir_vecs[0, i, :, :], dataset=dataset)
    # print(poses)
    print(dir_vecs)

    expected_pose_1 = np.array([
        [0., 0., 1],
        [0.28734789, 0., 0.95782629],
        [-0.28734789, 0., 0.95782629],
        [0., -1., 0.],
        [0., 0., -1.],
        [0., 0., -1.],
        [0., 1., 0.],
        [0., 0., -1.],
        [0., 0., -1.]
    ])
    assert_array_almost_equal(expected_pose_1, dir_vecs[0, 0, :, :])
    
    expected_pose_2 = np.array([
        [0., 0., 1],
        [0.28734789, 0., 0.95782629],
        [-0.28734789, 0., 0.95782629],
        [0., -1., 0.],
        [0., 0., -1.],
        [np.sqrt(2)/2, np.sqrt(2)/2, 0.],
        [0., 1., 0.],
        [0., 0., -1.],
        [np.sqrt(2)/2, -np.sqrt(2)/2, 0.]
    ])
    assert_array_almost_equal(expected_pose_2, dir_vecs[0, 1, :, :])

    expected_pose_3 = np.array([
        [0., 0., 1],
        [0.28734789, 0., 0.95782629],
        [-0.28734789, 0., 0.95782629],
        [0., -1., 0.],
        [0., 0., -1.],
        [np.sqrt(2)/2, -np.sqrt(2)/2, 0.],
        [0., 1., 0.],
        [0., 0., -1.],
        [np.sqrt(2)/2, np.sqrt(2)/2, 0.]
    ])
    assert_array_almost_equal(expected_pose_3, dir_vecs[0, 2, :, :])

    expected_pose_4 = np.array([
        [0., 0., 1],
        [0.28734789, 0., 0.95782629],
        [-0.28734789, 0., 0.95782629],
        [0., -1., 0.],
        [0.5, -np.sqrt(2)/2, -0.5],
        [0.5, -np.sqrt(2)/2, -0.5],
        [0., 1., 0.],
        [0.5, np.sqrt(2)/2, -0.5],
        [0.5, np.sqrt(2)/2, -0.5]
    ])
    assert_array_almost_equal(expected_pose_4, dir_vecs[0, 3, :, :])

    expected_pose_5 = np.array([
        [0., 0., 1],
        [0.28734789, 0., 0.95782629],
        [-0.28734789, 0., 0.95782629],
        [0., -1., 0.],
        [0.5, -np.sqrt(2)/2, -0.5],
        [0.5, -np.sqrt(2)/2, -0.5],
        [0., 1., 0.],
        [0.5, np.sqrt(2)/2, -0.5],
        [0.5, np.sqrt(2)/2, -0.5]
    ])
    assert_array_almost_equal(expected_pose_5, dir_vecs[0, 4, :, :])

    expected_pose_6 = np.array([
        [0., 0., 1],
        [0.28734789, 0., 0.95782629],
        [-0.28734789, 0., 0.95782629],
        [0., -1., 0.],
        [np.sqrt(3/8), -0.5, -np.sqrt(3/8)],
        [np.sqrt(3/8), -0.5, -np.sqrt(3/8)],
        [0., 1., 0.],
        [np.sqrt(3/8), 0.5, -np.sqrt(3/8)],
        [np.sqrt(3/8), 0.5, -np.sqrt(3/8)]
    ])
    assert_array_almost_equal(expected_pose_6, dir_vecs[0, 5, :, :])

    expected_pose_7 = np.array([
        [0., 0., 1],
        [0.28734789, 0., 0.95782629],
        [-0.28734789, 0., 0.95782629],
        [0., -1., 0.],
        [np.sqrt(3/8), -0.5, -np.sqrt(3/8)],
        [0.25, -np.sqrt(3/8), 0.75],
        [0., 1., 0.],
        [np.sqrt(3/8), 0.5, -np.sqrt(3/8)],
        [0.75, -np.sqrt(3/8), 0.25]
    ])
    assert_array_almost_equal(expected_pose_7, dir_vecs[0, 6, :, :])

    expected_pose_8 = np.array([
        [0., 0., 1],
        [0.28734789, 0., 0.95782629],
        [-0.28734789, 0., 0.95782629],
        [0., -1., 0.],
        [0.498097349, 0.0871557427, -0.862729916],
        [0.752692873, 0.653620045, -0.0789574274],
        [0., 1., 0.],
        [-np.sqrt(1/8), np.sqrt(2)/2,  -np.sqrt(3/8)],
        [0.362372436, -0.5, -0.786566092]
    ])
    assert_array_almost_equal(expected_pose_8, dir_vecs[0, 7, :, :])

    expected_pose_9 = np.array([
        [0., 0., 1],
        [0.28734789, 0., 0.95782629],
        [-0.28734789, 0., 0.95782629],
        [0., -1., 0.],
        [-0.5, -np.sqrt(3/4), 0.],
        [0.280330086, -0.739198920, 0.612372436],
        [0., 1., 0.],
        [np.sqrt(3/8), np.sqrt(2)/2, np.sqrt(1/8)],
        [0.375, -0.0794593113,  0.923613132]
    ])
    assert_array_almost_equal(expected_pose_9, dir_vecs[0, 8, :, :])

    if not plot_from_dir_vec:
        dir_vecs = None
    if display:
        play_arm_gestures.display_animate(poses=poses,
                                          names=['Test'],
                                          dir_vec=dir_vecs,
                                          save=save,
                                          video_name='0',
                                          azim=140,
                                          elev=-150)

test = test_compute_dir_vec_from_angle_pos(display=True, dataset='ted')
