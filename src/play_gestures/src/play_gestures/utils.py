#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sklearn.preprocessing import normalize
import numpy as np
import re

dir_vec_pairs_ted = [(0, 1, 0.26), (1, 2, 0.18), (2, 3, 0.14), (1, 4, 0.22), (4, 5, 0.36),
                     (5, 6, 0.33), (1, 7, 0.22), (7, 8, 0.36), (8, 9, 0.33)]  # adjacency and bone length
dir_vec_pairs_genea_2022_org = [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6), (6, 7), (7, 8), (8,9),
                 (3, 10), (10, 11), (11, 12), (12, 13), (13, 14), (14, 15), (3, 16), (16, 17)]  # Genea 20222 dataset without root 18 joints
dir_vec_pairs_genea_2022 = [(0, 1, 0.36963), (1, 2, 0.39087), (2, 3, 0.51221), (3, 4, 0.49585), (1, 5, 0.38623), (5, 6, 0.51221), (6, 7, 0.49585), (1, 8, 0.26392), (8, 9, 0.14209)]  # Genea 20222 dataset without root 10 joints


def normalize_string(s):
    """ lowercase, trim, and remove non-letter characters """
    s = s.lower().strip()
    # s = re.sub(r"([,.!?])", r" \1 ", s)  # isolate some marks
    s = re.sub(r"(['])", r"", s)  # remove apostrophe
    s = re.sub(r"[^a-zA-Z,.!?]+", r" ", s)  # replace other characters with whitespace
    s = re.sub(r"\s+", r" ", s).strip()
    return s


def convert_dir_vec_to_pose(vec, dataset):
    if dataset == 'ted':
        dir_vec_pairs = dir_vec_pairs_ted
    elif dataset == 'genea_2022':
        dir_vec_pairs = dir_vec_pairs_genea_2022
    elif dataset == 'genea_2022_org':
        dir_vec_pairs = dir_vec_pairs_genea_2022_org

    vec = np.array(vec)

    if vec.shape[-1] != 3:
        vec = vec.reshape(vec.shape[:-1] + (-1, 3))

    if len(vec.shape) == 2:
        joint_pos = np.zeros((10, 3))
        for j, pair in enumerate(dir_vec_pairs):
            joint_pos[pair[1]] = joint_pos[pair[0]] + pair[2] * vec[j]
    elif len(vec.shape) == 3:
        joint_pos = np.zeros((vec.shape[0], 10, 3))
        for j, pair in enumerate(dir_vec_pairs):
            joint_pos[:, pair[1]] = joint_pos[:, pair[0]] + pair[2] * vec[:, j]
    elif len(vec.shape) == 4:  # (batch, seq, 9, 3)
        joint_pos = np.zeros((vec.shape[0], vec.shape[1], 10, 3))
        for j, pair in enumerate(dir_vec_pairs):
            joint_pos[:, :, pair[1]] = joint_pos[:, :, pair[0]] + pair[2] * vec[:, :, j]
    else:
        assert False

    return joint_pos


def convert_pose_seq_to_dir_vec(pose, dataset):
    if dataset == 'ted':
        dir_vec_pairs = dir_vec_pairs_ted
    elif dataset == 'genea_2022':
        dir_vec_pairs = dir_vec_pairs_genea_2022
    elif dataset == 'genea_2022_org':
        dir_vec_pairs = dir_vec_pairs_genea_2022_org

    if pose.shape[-1] != 3:
        pose = pose.reshape(pose.shape[:-1] + (-1, 3))

    if len(pose.shape) == 3:
        dir_vec = np.zeros((pose.shape[0], len(dir_vec_pairs), 3))
        for i, pair in enumerate(dir_vec_pairs):
            dir_vec[:, i] = pose[:, pair[1]] - pose[:, pair[0]]
            dir_vec[:, i, :] = normalize(dir_vec[:, i, :], axis=1)  # to unit length
    elif len(pose.shape) == 4:  # (batch, seq, ...)
        dir_vec = np.zeros((pose.shape[0], pose.shape[1], len(dir_vec_pairs), 3))
        for i, pair in enumerate(dir_vec_pairs):
            dir_vec[:, :, i] = pose[:, :, pair[1]] - pose[:, :, pair[0]]
        for j in range(dir_vec.shape[0]):  # batch
            for i in range(len(dir_vec_pairs)):
                dir_vec[j, :, i, :] = normalize(dir_vec[j, :, i, :], axis=1)  # to unit length
    else:
        assert False

    return dir_vec


def constraint_angle(angle, min_value=-np.pi, max_value=np.pi):
    length = max_value - min_value

    # if angle > max_value:
    #     diff = angle - max_value
    #     new_angle = min_value + (diff % length)
    # elif angle < min_value:
    #     diff = min_value - angle
    #     new_angle = max_value - (diff % length)
    # else:
    #     new_angle = angle
    new_angle = np.where(angle > max_value, min_value + ((angle - max_value) % length), angle)
    new_angle = np.where(angle < min_value, max_value - ((min_value - angle) % length), new_angle)
    return new_angle


def Ry(theta):
  return np.array([[ np.cos(theta), 0., np.sin(theta)],
                    [0., 1. , 0.],
                    [-np.sin(theta), 0., np.cos(theta)]])


def Rz(theta):
  return np.array([[ np.cos(theta), -np.sin(theta), 0.],
                    [ np.sin(theta), np.cos(theta) , 0.],
                    [0., 0., 1.]])


def Rx(theta):
  return np.array([[1., 0., 0.],
                    [0., np.cos(theta),-np.sin(theta)],
                    [0, np.sin(theta), np.cos(theta)]])


def Rot_x(theta):
  return np.array([
    [1., 0., 0., 0.],
    [0., np.cos(theta),-np.sin(theta), 0.],
    [0, np.sin(theta), np.cos(theta), 0.],
    [0., 0., 0., 1.]
    ])


def Rot_z(theta):
  return np.array([
    [ np.cos(theta), -np.sin(theta), 0., 0.],
    [ np.sin(theta), np.cos(theta) , 0., 0.],
    [0., 0., 1., 0.],
    [0., 0., 0., 1.]
    ])


def Rot_y(theta):
  return np.array([
    [ np.cos(theta), 0., np.sin(theta), 0.],
    [0., 1. , 0., 0.],
    [-np.sin(theta), 0., np.cos(theta), 0.],
    [0., 0. , 0., 1.]
    ])


EPSILON = 1e-4
def exponential_space(start, stop, num):
    if start == 0.:
        start += EPSILON
    if stop == 0.:
        stop += EPSILON
    start = np.log(start)
    stop = np.log(stop)
    return np.exp(np.linspace(start, stop, num=num))


def log_space(start, stop, num):
    start = np.exp(start)
    stop = np.exp(stop)
    return np.log(np.linspace(start, stop, num=num))


def power_space(start, stop, power, num):
    start = np.power(start, 1/float(power))
    stop = np.power(stop, 1/float(power))
    return np.power( np.linspace(start, stop, num=num), power) 