#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from play_gestures.utils import Rx, Ry, Rz, constraint_angle
import time
import os


class PlayArmGestures:
    def __init__(self, dataset):
        # self.dir_vec_pairs = [(0, 1, 0.26), (1, 2, 0.18), (2, 3, 0.14), (1, 4, 0.22), (4, 5, 0.36),
        #          (5, 6, 0.33), (1, 7, 0.22), (7, 8, 0.36), (8, 9, 0.33)]  # adjacency and bone length
        self.dir_vec_pairs_ted = [(0, 1, 0.26), (1, 2, 0.18), (2, 3, 0.14), (1, 4, 0.21), (4, 5, 0.215),
                 (5, 6, 0.32), (1, 7, 0.21), (7, 8, 0.215), (8, 9, 0.32)]  # adjacency and bone length with ari values
        self.dir_vec_pairs_genea_2022 = [(0, 1, 0.26), (1, 8, 0.18), (8, 9, 0.14), (1, 2, 0.21), (2, 3, 0.215),
                 (3, 4, 0.32), (1, 5, 0.21), (5, 6, 0.215), (6, 7, 0.32)]  # adjacency and bone length with ari values
        self.dataset = dataset
        if self.dataset == 'ted':
            self.dir_vec_pairs = self.dir_vec_pairs_ted
        if self.dataset == 'genea_2022':
            self.dir_vec_pairs = self.dir_vec_pairs_genea_2022
        self.n_points = 7
        self.n_joints = 8
        self.n_limbs = 9
        self.n_dir_vec = 5
        # Values got from URDF files
        self.j1_max = np.radians(120.)
        self.j1_min = np.radians(-120.)
        self.j2_max = np.radians(155.)
        self.j2_min = np.radians(-5.)
        self.j3_max = np.radians(120.)
        self.j3_min = np.radians(-120.)
        self.j4_max = np.radians(135.)
        self.j4_min = np.radians(-5.)
        self.j1_vel_max = 1.95
        self.j2_vel_max = 1.95
        self.j3_vel_max = 2.35
        self.j4_vel_max = 2.35
        self.j1_acc_max = 10./0.730376620707115  # f = m.a Newton's Second Law
        self.j2_acc_max = 10./0.5891913
        self.j3_acc_max = 5./0.4650332
        self.j4_acc_max = 5./0.5958179


    def display_animate(self, poses, names, dir_vec=None, save=False, video_name='default_name', azim=65, elev=-65, superimpose=False):
        n_frames = poses[0].shape[0]
        fig = plt.figure(figsize=(15, 12))
        len_poses = len(poses)
        if superimpose:
            ax = fig.add_subplot(1, 1, 1, projection='3d')
            ani = animation.FuncAnimation(fig, self.animate_superimpose, frames=n_frames, fargs=(ax, poses, names, dir_vec, azim, elev), repeat=False)
        else:
            if len_poses == 1:
                axes = [fig.add_subplot(1, 1, idx + 1, projection='3d') for idx in range(len_poses)]
            elif len_poses == 2:
                axes = [fig.add_subplot(1, 2, idx + 1, projection='3d') for idx in range(len_poses)]
            elif len_poses == 4:
                axes = [fig.add_subplot(2, 2, idx + 1, projection='3d') for idx in range(len_poses)]
            elif len_poses == 6:
                axes = [fig.add_subplot(2, 3, idx + 1, projection='3d') for idx in range(len_poses)]
            elif len_poses== 8:
                axes = [fig.add_subplot(2, 4, idx + 1, projection='3d') for idx in range(len_poses)]
            elif len_poses== 9:
                axes = [fig.add_subplot(3, 3, idx + 1, projection='3d') for idx in range(len_poses)]
            elif len_poses== 10:
                axes = [fig.add_subplot(2, 5, idx + 1, projection='3d') for idx in range(len_poses)]
            else:
                raise ValueError("poses length should have length of 1, 2, 4, 6, 8, 9 or 10 but has length of {}".format(len_poses))
            ani = animation.FuncAnimation(fig, self.animate, frames=n_frames, fargs=(axes, poses, names, dir_vec, azim, elev), repeat=False)
            # self.animate(0, axes, poses, names, dir_vec, azim, elev)
        plt.tight_layout()
        
        if save:
            start_time = time.time()
            video_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/generation/{}.mp4'.format(video_name)
            if not os.path.isfile(video_path):                
                try:
                    ani.save(video_path, fps=15, dpi=80)  # dpi 150 for a higher resolution
                    del ani
                    plt.close(fig)
                except RuntimeError:
                    assert False, 'RuntimeError'
            print('Done, took {:.1f} seconds to save the video'.format(time.time() - start_time))
        else:        
            plt.show()


    def animate(self, i, axes, list_poses, names, dir_vec=None, azim=65, elev=-65):
        if len(list_poses) != len(axes):
            raise ValueError("list_poses and axes should have the same legnth, but have length of {} and {} respectively".format(len(list_poses), len(axes)))
        for ax in axes:
            ax.clear()
            ax.set_xlim3d(-0.5, 0.5)
            ax.set_ylim3d(0.5, -0.5)
            ax.set_zlim3d(0.5, -0.5)   
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('z')
            ax.view_init(azim=azim, elev=elev)
        for idx_ax, poses in enumerate(list_poses):
            for idx, point_3d in enumerate(poses[i]):
                axes[idx_ax].scatter(*point_3d, color='k')
                axes[idx_ax].text(*point_3d, idx)
                axes[idx_ax].set_title('Speaker id : {}'.format(names[idx_ax]))

            if dir_vec is None:
                for idx_pairs, pair in enumerate(self.dir_vec_pairs):
                    color = 'C{}'.format(idx_pairs)
                    axes[idx_ax].plot([poses[i][pair[0], 0], poses[i][pair[1], 0]],
                                      [poses[i][pair[0], 1], poses[i][pair[1], 1]],
                                      [poses[i][pair[0], 2], poses[i][pair[1], 2]],
                                       zdir='z',
                                       color=color)
            else:
                if self.dataset == 'ted':
                    for idx_dir_vec, vec in enumerate(dir_vec[idx_ax][i]):
                        color = 'C{}'.format(idx_dir_vec)
                        if idx_dir_vec == 3:
                            axes[idx_ax].quiver(*poses[i][1], *vec, length=0.3, color=color)
                        elif idx_dir_vec == 6:
                            axes[idx_ax].quiver(*poses[i][1], *vec, length=0.3, color=color)
                        else:
                            axes[idx_ax].quiver(*poses[i][idx_dir_vec], *vec, length=0.3, color=color)

                    new_l_r_shoulder = poses[i][4] - poses[i][7]
                    axes[idx_ax].quiver(*poses[i][7], *new_l_r_shoulder, length=1, color='C{}'.format(idx_dir_vec + 1))
                
                if self.dataset == 'genea_2022':
                    vec = dir_vec[idx_ax][i][0]
                    color = 'C{}'.format(0)
                    axes[idx_ax].quiver(*poses[i][0], *vec, length=0.3, color=color)

                    vec = dir_vec[idx_ax][i][7]
                    color = 'C{}'.format(1)
                    axes[idx_ax].quiver(*poses[i][1], *vec, length=0.3, color=color)

                    vec = dir_vec[idx_ax][i][8]
                    color = 'C{}'.format(2)
                    axes[idx_ax].quiver(*poses[i][8], *vec, length=0.3, color=color)

                    vec = dir_vec[idx_ax][i][1]
                    color = 'C{}'.format(3)
                    axes[idx_ax].quiver(*poses[i][1], *vec, length=0.3, color=color)

                    vec = dir_vec[idx_ax][i][2]
                    color = 'C{}'.format(4)
                    axes[idx_ax].quiver(*poses[i][2], *vec, length=0.3, color=color)

                    vec = dir_vec[idx_ax][i][3]
                    color = 'C{}'.format(5)
                    axes[idx_ax].quiver(*poses[i][3], *vec, length=0.3, color=color)

                    vec = dir_vec[idx_ax][i][4]
                    color = 'C{}'.format(6)
                    axes[idx_ax].quiver(*poses[i][1], *vec, length=0.3, color=color)

                    vec = dir_vec[idx_ax][i][5]
                    color = 'C{}'.format(7)
                    axes[idx_ax].quiver(*poses[i][5], *vec, length=0.3, color=color)

                    vec = dir_vec[idx_ax][i][6]
                    color = 'C{}'.format(8)
                    axes[idx_ax].quiver(*poses[i][6], *vec, length=0.3, color=color)

                    new_l_r_shoulder = poses[i][2] - poses[i][5]
                    axes[idx_ax].quiver(*poses[i][5], *new_l_r_shoulder, length=1, color='C{}'.format(9))


    def animate_superimpose(self, i, ax, list_poses, names, dir_vec=None, azim=65, elev=-65):
        ax.clear()
        ax.set_xlim3d(-0.5, 0.5)
        ax.set_ylim3d(0.5, -0.5)
        ax.set_zlim3d(0.5, -0.5)   
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.view_init(azim=azim, elev=elev)
        plot_label = [False]*len(list_poses)
        for idx_ax, poses in enumerate(list_poses):
            color = 'C{}'.format(idx_ax)
            for idx, point_3d in enumerate(poses[i]):
                ax.scatter(*point_3d, color='k')
                ax.text(*point_3d, idx)

            if dir_vec is None:
                for pair in self.dir_vec_pairs:
                    if not plot_label[idx_ax]:
                        ax.plot([poses[i][pair[0], 0], poses[i][pair[1], 0]],
                                [poses[i][pair[0], 1], poses[i][pair[1], 1]],
                                [poses[i][pair[0], 2], poses[i][pair[1], 2]],
                                zdir='z',
                                color=color,
                                label='Speaker id : {}'.format(names[idx_ax]))
                        plot_label[idx_ax] = True
                    else:
                        ax.plot([poses[i][pair[0], 0], poses[i][pair[1], 0]],
                            [poses[i][pair[0], 1], poses[i][pair[1], 1]],
                            [poses[i][pair[0], 2], poses[i][pair[1], 2]],
                            zdir='z',
                            color=color)

            else:
                for idx_dir_vec, vec in enumerate(dir_vec[idx_ax][i]):
                    if idx_dir_vec == 3:
                        ax.quiver(*poses[i][1], *vec, length=0.3, color=color)
                    elif idx_dir_vec == 6:
                        ax.quiver(*poses[i][1], *vec, length=0.3, color=color)
                    else:
                        ax.quiver(*poses[i][idx_dir_vec], *vec, length=0.3, color=color)

                new_l_r_shoulder = poses[i][4] - poses[i][7]
                ax.quiver(*poses[i][7], *new_l_r_shoulder, length=1, color=color, label='Speaker id : {}'.format(names[idx_ax]))
        ax.legend()


    def compute_dir_vec_from_angle_pos(self, joints):
        _, joints = self.check_joint_limits(joints)
        lj1, lj2, lj3, lj4, rj1, rj2, rj3, rj4 = joints
        LNS = [0, 1, 0]
        RNS = [0, -1, 0]
        S_N = [0, 0, 1]
        N_NO = [0.3, 0, 1]
        NO_H = [-0.3, 0, 1]
        LSE = [0, 0, -1]
        LEH = [1, 1, -1]
        RSE = [0, 0, -1]
        REH = [1, 0, -1]
        z = [0., 0., -1]
        # lj1 = np.pi/4
        # lj2 = np.pi/4
        # rj1 = lj1
        # rj2 = lj2
        # lj3 = np.pi/4
        # lj4 = np.pi/2
        # rj3 = lj3
        # rj4 = lj4
        LSE = Ry(-lj1) @ Rx(lj2) @ z
        RSE = Ry(-rj1) @ Rx(-rj2) @ z
        # LEH = Ry(-lj4) @ Rx(lj3) @ LSE
        # LEH = Rz(lj3) @ Ry(-lj4) @ LSE
        LEH = Ry(-lj1) @ Rx(lj2) @ Rz(lj3) @ Ry(-lj4) @ z
        # REH = Ry(-rj4) @ Rx(-rj3) @ RSE
        # REH = Rz(-rj3) @ Ry(-rj4) @ RSE
        REH = Ry(-rj1) @ Rx(-rj2) @ Rz(-rj3) @ Ry(-rj4) @ z
        nLNS = LNS/np.linalg.norm(LNS)
        nRNS = RNS/np.linalg.norm(RNS)
        nS_N = S_N/np.linalg.norm(S_N)
        nN_NO = N_NO/np.linalg.norm(N_NO)
        nNO_H = NO_H/np.linalg.norm(NO_H)
        nLSE = LSE/np.linalg.norm(LSE)
        nLEH = LEH/np.linalg.norm(LEH)
        nRSE = RSE/np.linalg.norm(RSE)
        nREH = REH/np.linalg.norm(REH)
        dir_vec = np.array([nS_N, nN_NO, nNO_H, nRNS, nRSE, nREH, nLNS, nLSE, nLEH])
        return dir_vec


    def compute_angle_pos_from_dir_vec(self, points=None, dir_vec=None) :
        """ 
        points contains a numpy array with those index/joints:
                    0= b_spine3
                    1= p_r_scap
                    2= b_r_forearm
                    3= b_r_wrist
                    4= p_l_scap
                    5= b_l_forearm
                    6= b_l_wrist
                    LRS = left right shoulder
                    LSE = left shoulder elbow
                    LEH = left elbow hand
                    RSE = right shoulder elbow
                    REH = right elbow hand
        """

        if points is not None:
            points = np.array(points)
            if not points.shape == (self.n_points, 3):
                raise ValueError('points should have a shape ({}, {}) but has shape {}'.format(self.n_points, 3, points.shape))
            LS = points[4]
            RS = points[1]
            LE = points[5]
            RE = points[2]
            LH = points[6]
            RH = points[3]
            # LRS = LS-RS
            LSE = LE-LS
            LEH = LH-LE
            RSE = RE-RS
            REH = RH-RE

        if dir_vec is not None:
            dir_vec = np.array(dir_vec)
            if not dir_vec.shape == (self.n_dir_vec, 3):
                raise ValueError('dir_vec should have a shape ({}, {}) but has shape {}'.format(self.n_dir_vec, 3, dir_vec.shape))
            LRS = dir_vec[0]
            LSE = dir_vec[1]
            LEH = dir_vec[2]
            RSE = dir_vec[3]
            REH = dir_vec[4]

        nLSE = LSE/np.linalg.norm(LSE)
        nLEH = LEH/np.linalg.norm(LEH)
        nRSE = RSE/np.linalg.norm(RSE)
        nREH = REH/np.linalg.norm(REH)
        x = np.array([1, 0, 0])

        lj1 = constraint_angle(np.pi/2 + np.arctan2(nLSE[2], nLSE[0]))
        if nLSE[2] == 0 and nLSE[1] == 0:
            lj2 = 0.
        else:
            lj2 = np.arcsin(nLSE[1]/np.sqrt(nLSE[2]**2 + nLSE[1]**2))
        LSEH = np.cross(nLSE, nLEH)
        if not LSEH.any():
            lj3 = 0.
        else:
            nLSEH = LSEH/np.linalg.norm(LSEH)
            x_left = Ry(-lj1) @ Rx(-lj2) @ x
            lj3 = np.pi/2 - np.arccos(np.dot(x_left, nLSEH))
        lj4 = np.arccos(np.dot(nLSE, nLEH))

        rj1 = constraint_angle(np.pi/2 + np.arctan2(nRSE[2], nRSE[0]))
        if nRSE[2] == 0 and nRSE[1] == 0:
            rj2 = 0.
        else:
            rj2 = -np.arcsin(nRSE[1]/np.sqrt(nRSE[2]**2 + nRSE[1]**2))
        RSEH = np.cross(nRSE, nREH)
        if not RSEH.any():
            rj3 = 0.
        else:
            nRSEH = RSEH/np.linalg.norm(RSEH)
            x_right = Ry(-rj1) @ Rx(-rj2) @ x
            rj3 = -np.pi/2 + np.arccos(np.dot(x_right, nRSEH))
        rj4 = np.arccos(np.dot(nRSE, nREH))

        joints  = [lj1, lj2, lj3, lj4, rj1, rj2, rj3, rj4]
        _, joints = self.check_joint_limits(joints)

        return joints

    
    def check_velocity_limits(self, veloctiy, joint_name):
        limit = False
        if joint_name == 'arm_right_1_joint' or joint_name == 'arm_left_1_joint':
            if np.abs(veloctiy) > self.j1_vel_max:
                veloctiy = np.sign(veloctiy) * self.j1_vel_max
                limit = True
        if joint_name == 'arm_right_2_joint' or joint_name == 'arm_left_2_joint':
            if np.abs(veloctiy) > self.j2_vel_max:
                veloctiy = np.sign(veloctiy) * self.j2_vel_max
                limit = True
        if joint_name == 'arm_right_3_joint' or joint_name == 'arm_left_3_joint':
            if np.abs(veloctiy) > self.j3_vel_max:
                veloctiy = np.sign(veloctiy) * self.j3_vel_max
                limit = True
        if joint_name == 'arm_right_4_joint' or joint_name == 'arm_left_4_joint':
            if np.abs(veloctiy) > self.j4_vel_max:
                veloctiy = np.sign(veloctiy) * self.j4_vel_max
                limit = True
        return limit, veloctiy


    def check_acceleration_limits(self, acceleration, joint_name):
        limit = False
        if joint_name == 'arm_right_1_joint' or joint_name == 'arm_left_1_joint':
            if np.abs(acceleration) > self.j1_acc_max:
                acceleration = np.sign(acceleration) * self.j1_acc_max
                limit = True
        if joint_name == 'arm_right_2_joint' or joint_name == 'arm_left_2_joint':
            if np.abs(acceleration) > self.j2_acc_max:
                acceleration = np.sign(acceleration) * self.j2_acc_max
                limit = True
        if joint_name == 'arm_right_3_joint' or joint_name == 'arm_left_3_joint':
            if np.abs(acceleration) > self.j3_acc_max:
                acceleration = np.sign(acceleration) * self.j3_acc_max
                limit = True
        if joint_name == 'arm_right_4_joint' or joint_name == 'arm_left_4_joint':
            if np.abs(acceleration) > self.j4_acc_max:
                acceleration = np.sign(acceleration) * self.j4_acc_max
                limit = True
        return limit, acceleration


    def check_joint_limits(self, joints):
        lj1, lj2, lj3, lj4, rj1, rj2, rj3, rj4 = joints
        limit = False
        if lj1 > self.j1_max:
            lj1 = self.j1_max
            limit = True
        if lj1 < self.j1_min:
            lj1 = self.j1_min
            limit = True
        if lj2 > self.j2_max:
            lj2 = self.j2_max
            limit = True
        if lj2 < self.j2_min:
            lj2 = self.j2_min
            limit = True
        if lj3 > self.j3_max:
            lj3 = self.j3_max
            limit = True
        if lj3 < self.j3_min:
            lj3 = self.j3_min
            limit = True
        if lj4 > self.j4_max:
            lj4 = self.j4_max
            limit = True
        if lj4 < self.j4_min:
            lj4 = self.j4_min
            limit = True
        if rj1 > self.j1_max:
            rj1 = self.j1_max
            limit = True
        if rj1 < self.j1_min:
            rj1 = self.j1_min
            limit = True
        if rj2 > self.j2_max:
            rj2 = self.j2_max
            limit = True
        if rj2 < self.j2_min:
            rj2 = self.j2_min
            limit = True
        if rj3 > self.j3_max:
            rj3 = self.j3_max
            limit = True
        if rj3 < self.j3_min:
            rj3 = self.j3_min
            limit = True
        if rj4 > self.j4_max:
            rj4 = self.j4_max
            limit = True
        if rj4 < self.j4_min:
            rj4 = self.j4_min
            limit = True
        return limit, [lj1, lj2, lj3, lj4, rj1, rj2, rj3, rj4]
