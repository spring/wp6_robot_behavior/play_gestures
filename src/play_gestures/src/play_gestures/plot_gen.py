#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import os
import sys
import re
import pickle
from itertools import combinations
from play_gestures.play_arm_gestures import PlayArmGestures
from utils import convert_dir_vec_to_pose, convert_pose_seq_to_dir_vec


def main(options=None):
    global_path = '/local_scratch/aauterna/Documents/Work/projects/Gesture-Generation-from-Trimodal-Context/output/generation_results/'
    data = {}
    combinations_n = 4
    video_name = 'ted_gen_2_vid_'
    video_name_find = video_name + '48[0-9]'
    save = False
    plot_compare_gen = False
    plot_gestures_played = False
    plot_from_dir_vec = False
    plot_gestures_gen = False

    if isinstance(options, (list, tuple, dict)):
        for item in options:
            if item.lower() == 'save':
                save = True
            if item.lower() == 'plot_compare_gen':
                plot_compare_gen = True
            if item.lower() == 'plot_gestures_played':
                plot_gestures_played = True
            if item.lower() == 'plot_gestures_gen':
                plot_gestures_gen = True

    if plot_gestures_gen:
        play_arm_gestures = PlayArmGestures(dataset='genea_2022')
        local_path = '/home/ros/ari_public_ws/src/play_gestures/config/data/'
        # gestures = np.load(open('{}gestures_{}.npy'.format(local_path, '22_06_2023_10_17_42'), 'rb')).reshape(-1, play_arm_gestures.n_joints + 1, 3)
        # gestures = np.load(open('{}target_vec.npy'.format(local_path), 'rb')).reshape(-1, play_arm_gestures.n_joints + 1, 3)
        gestures = np.load(open('{}target_vec_1.npy'.format(local_path), 'rb')).reshape(-1, play_arm_gestures.n_joints + 1, 3)
        # gestures = np.load(open('{}target_poses.npy'.format(local_path), 'rb')).reshape(-1, play_arm_gestures.n_joints + 2, 3)
        # gestures = np.load(open('{}target_poses_1.npy'.format(local_path), 'rb')).reshape(-1, play_arm_gestures.n_joints + 2, 3)
        # gestures = np.load(open('{}sample_dataset_poses.npy'.format(local_path), 'rb'))
        # gestures = np.load(open('{}sample_dataset_dir_vec.npy'.format(local_path), 'rb'))
        # gestures = np.load(open('{}target_vec_ted.npy'.format(local_path), 'rb')).reshape(-1, play_arm_gestures.n_joints + 1, 3)
        # gestures = np.load(open('{}target_poses_ted.npy'.format(local_path), 'rb')).reshape(-1, play_arm_gestures.n_joints + 2, 3)
        mean_dir_vec = np.array([-0.01660, -0.99101, -0.09250, -0.96635, -0.09992, 0.14040, -0.18765, 0.94844, 0.10040, -0.00691, 0.60245, 0.49706, 0.95714, -0.12427, 0.15668, 0.15887, 0.94644, 0.15432, 0.06968, 0.59371, 0.47072, -0.01881, -0.96075, 0.24641, -0.00252, -0.84802, 0.47761])  # genea_2022
        mean_pose = np.array([0.00000, 0.00000, 0.00000, -0.00614, -0.36623, -0.03417, -0.38346, -0.40540, 0.02184, -0.47954, 0.08020, 0.07325, -0.48297, 0.37902, 0.31979, 0.36344, -0.41410, 0.02695, 0.44478, 0.07047, 0.10596, 0.47934, 0.36495, 0.33944, -0.01109, -0.61989, 0.03087, -0.01146, -0.74042, 0.09876])  # genea_2022
        mean_dir_vec_ted = np.array([ 0.0154009, -0.9690125, -0.0884354, -0.0022264, -0.8655276, 0.4342174, -0.0035145, -0.8755367, -0.4121039, -0.9236511, 0.3061306, -0.0012415, -0.5155854,  0.8129665,  0.0871897, 0.2348464,  0.1846561,  0.8091402,  0.9271948,  0.2960011, -0.013189 ,  0.5233978,  0.8092403,  0.0725451, -0.2037076, 0.1924306,  0.8196916])  # ted_dataset
        mean_pose_ted = np.array([ 0.0000306,  0.0004946,  0.0008437,  0.0033759, -0.2051629, -0.0143453,  0.0031566, -0.3054764,  0.0411491,  0.0029072, -0.4254303, -0.001311 , -0.1458413, -0.1505532, -0.0138192, -0.2835603,  0.0670333,  0.0107002, -0.2280813,  0.112117 , 0.2087789,  0.1523502, -0.1521499, -0.0161503,  0.291909 , 0.0644232,  0.0040145,  0.2452035,  0.1115339,  0.2051307])  # ted_dataset

        mean_dir_vec = mean_dir_vec.reshape(-1, 3)
        mean_dir_vec_ted = mean_dir_vec_ted.reshape(-1, 3)
        # print(gestures.shape, mean_dir_vec.shape)
        gestures += mean_dir_vec
        # gestures += mean_dir_vec_ted
        print(gestures.shape, mean_dir_vec.shape)
        # poses = np.zeros((1, gestures.shape[0], 10, 3))
        # for idx, gesture in enumerate(gestures):
        #     poses[0, idx, :, :] = convert_dir_vec_to_pose(gesture.flatten(), dataset='genea_2022')
        # poses = convert_dir_vec_to_pose(gestures, dataset='genea_2022')
        # poses = convert_dir_vec_to_pose(gestures, dataset='genea_2022')
        # gestures = convert_pose_seq_to_dir_vec(poses, dataset='genea_2022')
        poses = convert_dir_vec_to_pose(gestures, dataset='genea_2022')
        # play_arm_gestures.display_animate(poses=[poses], names='test', dir_vec=None, save=save, video_name='test')
        play_arm_gestures.display_animate(poses=[poses], names='test', dir_vec=[gestures], save=save, video_name='test')
        # play_arm_gestures.display_animate(poses=[gestures], names='test', dir_vec=None, save=save, video_name='test')
    
    if plot_compare_gen:
        play_arm_gestures = PlayArmGestures()
        for file in os.listdir(global_path):
            if file.endswith(".pkl"):
                match = re.findall(video_name_find, file)
                if match:
                    file_name = match[0]
                    pickle_data = pickle.load(open(global_path + file_name + '.pkl', 'rb'))
                    if plot_from_dir_vec:
                        data[file_name] = [pickle_data['out_dir_vec'].reshape(-1, play_arm_gestures.n_joints + 1, 3), pickle_data['out_poses']]
                    else:
                        data[file_name] = pickle_data['out_poses']

        for data_names in combinations(data.keys(), combinations_n):
            print(data_names)
            poses = []
            dir_vecs = []
            speaker_ids = []
            for data_name in data_names:                
                speaker_id = re.sub(video_name + '?', '', data_name)                
                if plot_from_dir_vec:
                    dir_vec_seq = data[data_name][0]
                    pose_seq = data[data_name][1]            
                    dir_vec_seq[:, :, 0] = -dir_vec_seq[:, :, 0]
                    dir_vecs.append(dir_vec_seq)
                else:
                    dir_vecs = None
                    pose_seq = data[data_name]
                pose_seq[:, :, 0] = -pose_seq[:, :, 0]
                if poses and poses[-1].shape != pose_seq.shape:
                    print('The last sequences of poses and the new one have a different shape, {} and {} respectively. So the new one was not added to the list to plot'.format(poses[-1].shape, pose_seq.shape))
                    continue
                poses.append(pose_seq)
                speaker_ids.append(speaker_id)
            vid_name = video_name + '_'.join(speaker_ids)
            play_arm_gestures.display_animate(poses, speaker_ids, dir_vec=dir_vecs, save=save, video_name=vid_name)
            break

    if plot_gestures_played:
        play_arm_gestures = PlayArmGestures()
        for file in os.listdir(global_path):
            if file.endswith(".npy"):
                match = re.findall(video_name + '(.+?).npy', file)
                if match and not 'sim' in file:
                    file_name = global_path + file
                    label = video_name + match[0].replace('delta_pose_', '')
                    speaker_id = re.findall('[0-9]+_?', match[0])[0][:-1]
                    data_np = np.load(open(file_name, 'rb'))
                    print(data_np.shape, label)
                    dir_vecs = []
                    poses = []
                    for data_i in range(data_np.shape[1]):
                        dir_vec = np.zeros((data_np.shape[0], play_arm_gestures.n_joints + 1, 3))
                        pose = np.zeros((data_np.shape[0], play_arm_gestures.n_joints + 2, 3))
                        for data_j in range(data_np.shape[0]):
                            dir_vec[data_j] = play_arm_gestures.compute_dir_vec_from_angle_pos(data_np[data_j, data_i, :])
                            pose[data_j] = convert_dir_vec_to_pose(vec=dir_vec[data_j], dir_vec_pairs=play_arm_gestures.dir_vec_pairs)
                        dir_vecs.append(dir_vec)
                        poses.append(pose)
                    if not plot_from_dir_vec:
                        dir_vecs = None
                    play_arm_gestures.display_animate(poses=poses,
                                                      names=['{} (Desired)'.format(speaker_id), '{} (Measured)'.format(speaker_id)],
                                                      dir_vec=dir_vecs,
                                                      save=save,
                                                      video_name='0',
                                                      azim=140,
                                                      elev=-150)
                    break


if __name__ == '__main__':
    options = None

    if len(sys.argv) > 1:
        options = sys.argv[1:]
    main(options=options)