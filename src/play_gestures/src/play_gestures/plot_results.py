#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')
import os
import re
import sys
import pickle
from play_gestures.play_arm_gestures import PlayArmGestures
from utils import convert_dir_vec_to_pose
from itertools import combinations


def main(options=None):
    save = False
    safe_cmd = False
    simulation = False
    global_analysis = False
    simple_plot = False
    plot_error = False
    plot_animation = False
    plot_from_dir_vec = False
    compute_metrics = False
    embed_space_viz = False
    superimpose = False

    data = {}
    joints = ['lj1', 'lj2', 'lj3', 'lj4', 'rj1', 'rj2', 'rj3', 'rj4']
    max_angle = np.radians([120., 155., 120., 135]*2)
    min_angle = np.radians([-120., -5., -120., -5]*2)
    offset = 2
    generation_results_path = '/local_scratch2/aauterna/output/generation_results/'
    play_gestures_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/'
    left_title = 'Left Joint Plot'
    right_title = 'Right Joint Plot'
    mae_title = 'Mean Average Error Plot'

    if isinstance(options, (list, tuple, dict)):
        for item in options:
            if item.lower() == 'save':
                save = True
            if item.lower() == 'global':
                global_analysis = True
            if item.lower() == 'simple':
                simple_plot = True
            if item.lower() == 'safe_cmd':
                safe_cmd = True
            if item.lower() == 'simulation':
                simulation == True
            if item.lower() == 'plot_error':
                plot_error = True
            if item.lower() == 'plot_animation':
                plot_animation = True
            if item.lower() == 'plot_from_dir_vec':
                plot_from_dir_vec = True
            if item.lower() == 'compute_metrics':
                compute_metrics = True
            if item.lower() == 'embed_space_viz':
                embed_space_viz = True
            if item.lower() == 'superimpose':
                superimpose = True

    if simple_plot:
        for file in os.listdir(generation_results_path):
            if file.endswith(".npy"):
                match = re.findall('vid_[0-9]+_delta', file)
                if match:
                    match = re.findall('[0-9]+', match[0])
                    if match:
                        label = '{}'.format(match[0])
                        if 'sim' in file:
                            label += '_sim'
                        file_name = os.path.join(generation_results_path, file)
                        data[label] = np.load(open(file_name, 'rb'))
                # match = re.findall('arm_(?:right|left)_[1-2-3-4]_joint_velocity_test_', file)
                match = re.findall('arm_(.+?)_delta_pose_', file)
                if match:
                    label = 'arm_{}'.format(match[0])
                    if 'sim' in file:
                        label += '_sim'
                    file_name = os.path.join(generation_results_path, file)
                    data[label] = np.load(open(file_name, 'rb'))

        for dirpath, dirnames, filenames in os.walk(play_gestures_path):
            for file in [f for f in filenames if f.endswith(".npy")]:
                match = re.findall('arm_(.+?)_delta_pose_', file)
                if match:
                    label = 'arm_{}'.format(match[0])
                    if 'sim' in file:
                        label += '_sim'
                    file_name = os.path.join(dirpath, file)
                    data[label] = np.load(open(file_name, 'rb'))
                match = re.findall('^[0-9]+', file)
                if match:
                    label = 'test_dataset_{}'.format(match[0])
                    if 'sim' in file:
                        label += '_sim'
                    file_name = os.path.join(dirpath, file)
                    data[label] = np.load(open(file_name, 'rb'))

        print(list(data.keys()), len(data.keys()))

        figures = [plt.figure(figsize=(10, 8)),
                plt.figure(figsize=(10, 8)),
                plt.figure(figsize=(10, 8))]
        for fig, fig_title in zip(figures, [left_title, right_title, mae_title]):    
            fig.canvas.manager.set_window_title(fig_title)

        axes = [figures[0].add_subplot(2, 2, 1),
                figures[0].add_subplot(2, 2, 2), 
                figures[0].add_subplot(2, 2, 3),
                figures[0].add_subplot(2, 2, 4),
                figures[1].add_subplot(2, 2, 1),
                figures[1].add_subplot(2, 2, 2), 
                figures[1].add_subplot(2, 2, 3),
                figures[1].add_subplot(2, 2, 4),
                figures[2].add_subplot(1, 1, 1)]

        for ax in axes:
            ax.clear()
            ax.set_xlabel('time step')
            ax.set_ylabel('angle (rad)')
            ax.grid(alpha=0.6)

        limit_angle_label = False
        desired_label = False

        for key, values in data.items():
            if 'test_dataset' in key and '512' in key:
                x = np.arange(values.shape[0]-offset)
                if offset:
                    error = np.abs(values[:-offset, 0, :] - values[offset:, 1, :])
                    desired = values[:-offset, 0, :]
                    measured = values[offset:, 1, :]
                    if 'test_dataset' in key:
                        mask = np.ones(error.shape[0], dtype=bool)
                        for idx in range(offset):
                            idx_to_rm = np.arange(34-(idx + 1), error.shape[0], 34)
                            mask[idx_to_rm] = False
                        desired = np.copy(desired[mask, :])
                        measured = np.copy(measured[mask, :])
                        error = np.copy(error[mask, :])
                        x = x[:-(idx_to_rm.shape[0]*offset)]
                else:
                    error = np.abs(values[:, 0, :] - values[:, 1, :])
                    desired = values[:, 0, :]
                    measured = values[:, 1, :]
                mean_average = np.mean(error)
                std_average = np.std(error)
                right_left_mean_average = np.mean(error, axis=1)
                print('{} {}'.format(mean_average, std_average))
                axes[-1].step(x, right_left_mean_average, label=key)
                axes[-1].set_title('Mean average error (over the 8 joints)')
                for idx, joint in enumerate(joints):
                    if not desired_label:
                        axes[idx].step(x, desired[:, idx], label='desired')
                    axes[idx].step(x, measured[:, idx], label='measured_'+key)
                    axes[idx].step(x, error[:, idx], label='error_'+key)
                    axes[idx].plot(x, [max_angle[idx]]*x.shape[0], color='silver')
                    if not limit_angle_label:
                        axes[idx].plot(x, [min_angle[idx]]*x.shape[0], color='silver', label='limit angle')
                    else:
                        axes[idx].plot(x, [min_angle[idx]]*x.shape[0], color='silver')
                    axes[idx].set_title(joint)
                desired_label = True
                limit_angle_label = True

        # for ax in axes:
            # ax.legend(loc="upper left")

        handles, labels = axes[0].get_legend_handles_labels()
        figures[0].legend(handles, labels, loc='upper left')
        handles, labels = axes[3].get_legend_handles_labels()
        figures[1].legend(handles, labels, loc='upper left')
        handles, labels = axes[-1].get_legend_handles_labels()
        figures[-1].legend(handles, labels, loc='upper left')
        for fig in figures:
            fig.tight_layout()
        plt.show()

    if global_analysis:
        play_arm_gestures = PlayArmGestures()
        data = {}
        len_analysis = 50
        data_gen = {}
        data_gt = {}
        gt_name = 'test_dataset'
        gen_name = 'generated_test_dataset'
        gen_save_model = '000194'  # '19_09_01' '000194' '000218' '000105' '000050'
        data_shape = []
        data['gt'] = data_gt
        data['gen'] = data_gen
        p = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]])
        j2_offset = False
        gt_date = '16_01_2023'  # '17_01_2023'
        gt_sim_date = '28_11_2022'  # '17_01_2023'
        gen_date = '16_01_2023'  # '16_01_2023' '24_11_2022'
        gen_sim_date = '16_01_2023'  # '16_01_2023' '28_11_2022'

        for dirpath, dirnames, filenames in os.walk(play_gestures_path):
            for file in [f for f in filenames if f.endswith(".npy")]:
                # get saved angles from the Test dataset
                match_gt = re.findall('^len_{}_date'.format(len_analysis) + '_\d{2}_\d{2}_\d{4}_\d{2}_\d{2}_\d{2}', file)

                # get saved angles from a Saved Model (old naming)
                match_gen_old = re.findall('ckpt_\d{2}_\d{2}_\d{2}' + '_len_{}_date'.format(len_analysis), file)

                # get saved angles from a Saved Model (new naming)
                match_gen_new = re.findall('ckpt_experiment_\d{6}' + '_len_{}_date'.format(len_analysis), file)

                if match_gt or match_gen_old or match_gen_new:
                    file_name = os.path.join(dirpath, file)
                    np_data = np.load(open(file_name, 'rb'))

                if match_gt:
                    dict_key = file.replace('len_{}_date_'.format(len_analysis), '').replace('.npy', '')

                if match_gen_old:
                    gen_model = match_gen_old[0].replace('ckpt_', '').replace('_len_{}_date'.format(len_analysis), '')
                    dict_key = file.replace('ckpt_{}_'.format(gen_model), '').replace('len_{}_date_'.format(len_analysis), '').replace('.npy', '')
                    if not data_gen:
                        data_gen[gen_model] = {}
                    elif gen_model not in list(data_gen.keys()):
                        data_gen[gen_model] = {}

                if match_gen_new:
                    gen_exp = re.findall('\d{6}', match_gen_new[0])
                    if gen_exp:
                        gen_model = gen_exp[0]
                        dict_key = file.replace('ckpt_experiment_{}_'.format(gen_model), '').replace('len_{}_date_'.format(len_analysis), '').replace('.npy', '')
                        if not data_gen:
                            data_gen[gen_model] = {}
                        elif gen_model not in list(data_gen.keys()):
                            data_gen[gen_model] = {}

                if match_gt or match_gen_old or match_gen_new:
                    data_shape.append(np_data.shape)
                    if match_gt:
                        data_gt[dict_key+'_ang'] = np_data
                    else:
                        data_gen[gen_model][dict_key+'_ang'] = np_data

        if not data_gt:
            raise ValueError("Robot saved angles from the Test Dataset were not found in the following directory of sub-directories : {}".format(play_gestures_path))
        if not data_gen:
            raise ValueError("Robot saved angles from a Saved Model name were not found in the following directory of sub-directories : {}".format(play_gestures_path))
        if not data_shape:
            raise ValueError("Any sequence in {} directory and sub-directories was recorded with a length of {}".format(play_gestures_path, len_analysis))
        else:
            data_shape = min(data_shape)
        
        # for key, val in data.items():
        #     if isinstance(val, dict):
        #         for key_i, val_i in val.items():
        #             if isinstance(val_i, dict):
        #                 for key_j, val_j in val_i.items():
        #                     print(key, key_i, key_j)
        #             else:
        #                 print(key, key_i)
        #     else:
        #                 print(key)

        for file in os.listdir(generation_results_path):
            if file.endswith(".pkl"):
                gen_model = [model for model in data_gen.keys() if model in file]
                if gen_model:
                    gen_model = gen_model[0]
                    print(file)
                    data_pickle = pickle.load(open('{}{}'.format(generation_results_path, file), 'rb'))
                    data_gen[gen_model]['model_dir_vec'] = data_pickle['out_dir_vec'].reshape(-1, data_pickle['out_dir_vec'].shape[-1])[:data_shape[0]]
                    data_gen[gen_model]['model_dir_vec'] = data_gen[gen_model]['model_dir_vec'].reshape(-1, play_arm_gestures.n_joints + 1, 3)
                    data_gen[gen_model]['model_dir_vec'][:, :, 0] = -data_gen[gen_model]['model_dir_vec'][:, :, 0]
                    data_gen[gen_model]['model_dir_vec'] = data_gen[gen_model]['model_dir_vec'] @ p
                    data_gt['gt_dir_vec'] = data_pickle['target_dir_vec'].reshape(-1, data_pickle['target_dir_vec'].shape[-1])[:data_shape[0]]
                    data_gt['gt_dir_vec'] = data_gt['gt_dir_vec'].reshape(-1, play_arm_gestures.n_joints + 1, 3)
                    data_gt['gt_dir_vec'][:, :, 0] = -data_gt['gt_dir_vec'][:, :, 0]
                    data_gt['gt_dir_vec'] = data_gt['gt_dir_vec'] @ p
                    data_gen[gen_model]['model_poses'] = data_pickle['out_poses'].reshape(-1, data_pickle['out_poses'].shape[-2], data_pickle['out_poses'].shape[-1])[:data_shape[0]]
                    data_gen[gen_model]['model_poses'][:, :, 0] = -data_gen[gen_model]['model_poses'][:, :, 0]
                    data_gen[gen_model]['model_poses'] = data_gen[gen_model]['model_poses'] @ p
                    data_gt['gt_poses'] = data_pickle['target_poses'].reshape(-1, data_pickle['target_poses'].shape[-2], data_pickle['target_poses'].shape[-1])[:data_shape[0]]
                    data_gt['gt_poses'][:, :, 0] = -data_gt['gt_poses'][:, :, 0]
                    data_gt['gt_poses'] = data_gt['gt_poses'] @ p

                    data_gt['gt_retargeted_dir_vec'] = np.zeros_like(data_gt['gt_dir_vec'])
                    data_gt['gt_retargeted_poses'] = np.zeros_like(data_gt['gt_poses'])
                    data_gt['gt_fixed_shoulders_dir_vec'] = np.copy(data_gt['gt_dir_vec'])
                    data_gt['gt_fixed_shoulders_poses'] = np.copy(data_gt['gt_poses'])
                    data_gen[gen_model]['model_retargeted_dir_vec'] = np.zeros_like(data_gen[gen_model]['model_dir_vec'])
                    data_gen[gen_model]['model_retargeted_poses'] = np.zeros_like(data_gen[gen_model]['model_poses'])
                    data_gen[gen_model]['model_fixed_shoulders_dir_vec'] = np.copy(data_gen[gen_model]['model_dir_vec'])
                    data_gen[gen_model]['model_fixed_shoulders_poses'] = np.copy(data_gen[gen_model]['model_poses'])
                    for idx_dir_vec, (dir_vec_model_tmp, dir_vec_gt_tmp) in enumerate(zip(data_gen[gen_model]['model_dir_vec'], data_gt['gt_dir_vec'])):
                        dir_vec_model_tmp_i = np.array([[0., 0., 0.],
                            dir_vec_model_tmp[-2],
                            dir_vec_model_tmp[-1],
                            dir_vec_model_tmp[-5],
                            dir_vec_model_tmp[-4]])
                        dir_vec_gt_tmp_i = np.array([[0., 0., 0.],
                            dir_vec_gt_tmp[-2],
                            dir_vec_gt_tmp[-1],
                            dir_vec_gt_tmp[-5],
                            dir_vec_gt_tmp[-4]])
                        joints_model = play_arm_gestures.compute_angle_pos_from_dir_vec(dir_vec=dir_vec_model_tmp_i)
                        joints_gt = play_arm_gestures.compute_angle_pos_from_dir_vec(dir_vec=dir_vec_gt_tmp_i)
                        data_gen[gen_model]['model_retargeted_dir_vec'][idx_dir_vec] = play_arm_gestures.compute_dir_vec_from_angle_pos(joints=joints_model)
                        data_gt['gt_retargeted_dir_vec'][idx_dir_vec] = play_arm_gestures.compute_dir_vec_from_angle_pos(joints=joints_gt)
                        data_gen[gen_model]['model_retargeted_poses'][idx_dir_vec] = convert_dir_vec_to_pose(vec=data_gen[gen_model]['model_retargeted_dir_vec'][idx_dir_vec], dir_vec_pairs=play_arm_gestures.dir_vec_pairs)
                        data_gt['gt_retargeted_poses'][idx_dir_vec] = convert_dir_vec_to_pose(vec=data_gt['gt_retargeted_dir_vec'][idx_dir_vec], dir_vec_pairs=play_arm_gestures.dir_vec_pairs)
                    
                    data_gt['gt_fixed_shoulders_dir_vec'][:, :4, :] = data_gt['gt_retargeted_dir_vec'][:, :4, :]
                    data_gt['gt_fixed_shoulders_dir_vec'][:, 6, :] = data_gt['gt_retargeted_dir_vec'][:, 6, :]
                    data_gen[gen_model]['model_fixed_shoulders_dir_vec'][:, :4, :] = data_gen[gen_model]['model_retargeted_dir_vec'][:, :4, :]
                    data_gen[gen_model]['model_fixed_shoulders_dir_vec'][:, 6, :] = data_gen[gen_model]['model_retargeted_dir_vec'][:, 6, :]
                    for idx_dir_vec, (dir_vec_model_tmp, dir_vec_gt_tmp) in enumerate(zip(data_gen[gen_model]['model_fixed_shoulders_dir_vec'], data_gt['gt_fixed_shoulders_dir_vec'])):
                        data_gen[gen_model]['model_fixed_shoulders_poses'][idx_dir_vec] = convert_dir_vec_to_pose(vec=dir_vec_model_tmp, dir_vec_pairs=play_arm_gestures.dir_vec_pairs)
                        data_gt['gt_fixed_shoulders_poses'][idx_dir_vec] = convert_dir_vec_to_pose(vec=dir_vec_gt_tmp, dir_vec_pairs=play_arm_gestures.dir_vec_pairs)

        # for key, val in data.items():
        #     if isinstance(val, dict):
        #         for key_i, val_i in val.items():
        #             if isinstance(val_i, dict):
        #                 for key_j, val_j in val_i.items():
        #                     print(key, key_i, key_j)
        #             else:
        #                 print(key, key_i)
        #     else:
        #                 print(key)
        
        for key, values in list(data.items()):
            if isinstance(values, dict):
                for i_key, i_values in list(values.items()):
                    if isinstance(i_values, dict):
                        for j_key, j_values in list(i_values.items()):
                            if 'ang' in j_key:
                                dir_vec = np.zeros((data_shape[0], play_arm_gestures.n_joints + 1, 3))
                                pose = np.zeros((data_shape[0], play_arm_gestures.n_joints + 2, 3))
                                for j in range(data_shape[0]):
                                    dir_vec[j] = play_arm_gestures.compute_dir_vec_from_angle_pos(j_values[j, 1, :])
                                    pose[j] = convert_dir_vec_to_pose(vec=dir_vec[j], dir_vec_pairs=play_arm_gestures.dir_vec_pairs)
                                data[key][i_key][j_key.replace('ang', 'poses')] = pose
                                data[key][i_key][j_key.replace('ang', 'dir_vec')] = dir_vec
                    else:
                        if 'ang' in i_key:
                            dir_vec = np.zeros((data_shape[0], play_arm_gestures.n_joints + 1, 3))
                            pose = np.zeros((data_shape[0], play_arm_gestures.n_joints + 2, 3))
                            for i in range(data_shape[0]):
                                dir_vec[i] = play_arm_gestures.compute_dir_vec_from_angle_pos(i_values[i, 1, :])
                                pose[i] = convert_dir_vec_to_pose(vec=dir_vec[i], dir_vec_pairs=play_arm_gestures.dir_vec_pairs)
                            data[key][i_key.replace('ang', 'poses')] = pose
                            data[key][i_key.replace('ang', 'dir_vec')] = dir_vec
        
        # for key, values in data.items():
        #     if isinstance(values, dict):
        #         for i_key, i_values in values.items():
        #             print(key, i_key)
        #             if isinstance(i_values, dict):
        #                 for j_key, j_values in i_values.items():
        #                     print(key, i_key, j_key)

        index_save_model = (list(data_gen.keys()).index(gen_save_model))
        gen_model = list(data_gen.keys())[index_save_model]

        for key, val in list(data['gt'].items()):
                if not 'sim' in key and 'dir_vec' in key and not 'offset' in key and not 'safe_cmd' in key and gt_date in key:
                    data['gt']['gt_robot_dir_vec'] = val
                if not 'sim' in key and 'poses' in key and not 'offset' in key and not 'safe_cmd' in key and gt_date in key:
                    data['gt']['gt_robot_poses'] = val
                if 'sim' in key and 'dir_vec' in key and not 'offset' in key and not 'safe_cmd' in key and gt_sim_date in key:
                    data['gt']['gt_sim_dir_vec'] = val
                if 'sim' in key and 'poses' in key and not 'offset' in key and not 'safe_cmd' in key and gt_sim_date in key:
                    data['gt']['gt_sim_poses'] = val
                if not 'sim' in key and 'ang' in key and not 'offset' in key and not 'safe_cmd' in key and gt_date in key:
                    data_gt['gt_robot_ang'] = val
                if 'sim' in key and 'ang' in key and not 'offset' in key and not 'safe_cmd' in key and gt_sim_date in key:
                    data_gt['gt_sim_ang'] = val
        
        for key, val in list(data['gen'][gen_model].items()):
                if not 'sim' in key and 'dir_vec' in key and not 'offset' in key and not 'safe_cmd' in key and gen_date in key:
                    data['gen'][gen_model]['gen_robot_dir_vec'] = val
                if 'sim' in key and 'dir_vec' in key and not 'offset' in key and not 'safe_cmd' in key and gen_sim_date in key:
                   data['gen'][gen_model]['gen_sim_dir_vec'] = val
                if not 'sim' in key and 'poses' in key and not 'offset' in key and not 'safe_cmd' in key and gen_date in key:
                    data['gen'][gen_model]['gen_robot_poses'] = val
                if 'sim' in key and 'poses' in key and not 'offset' in key and not 'safe_cmd' in key and gen_sim_date in key:
                    data['gen'][gen_model]['gen_sim_poses'] = val
                if not 'sim' in key and 'ang' in key and not 'offset' in key and not 'safe_cmd' in key and gen_date in key:
                    data_gen[gen_model]['gen_robot_ang'] = val
                if 'sim' in key and 'ang' in key and not 'offset' in key and not 'safe_cmd' in key and gen_sim_date in key:
                    data_gen[gen_model]['gen_sim_ang'] = val

        # for key, val in data.items():
        #     if isinstance(val, dict):
        #         for key_i, val_i in val.items():
        #             if isinstance(val_i, dict):
        #                 for key_j, val_j in val_i.items():
        #                     print(key_i, key_j)
        #             else:
        #                 print(key_i)
        #     else:
        #                 print(key)

        if compute_metrics:
            import torch
            import torch.nn as nn
            from gestures_generation.model.embedding_space_evaluator import EmbeddingSpaceEvaluator
            from gestures_generation.utils.train_utils import load_config
            from gestures_generation.utils.vocab_utils import build_vocab
            device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
            match_new = re.findall('\d{6}', gen_model)
            match_old = re.findall('\d{2}_\d{2}_\d{2}', gen_model)
            if match_new:
                checkpoint_path = '/local_scratch2/aauterna/output/runs/saved_models/{}/multimodal_context_checkpoint_best.bin'.format(match_new[0])
                checkpoint_data = torch.load(checkpoint_path, map_location=device)
                args = checkpoint_data['args']
                lang_model = checkpoint_data['lang_model']
                out_dim = checkpoint_data['pose_dim']
                if args.eval_net_path and len(args.eval_net_path) > 0:
                    eval_net_path = '/local_scratch/aauterna/Documents/Work/projects/gestures_generation/code/gestures_generation/data/{}'.format(args.eval_net_path[-70:])
                    embed_space_evaluator = EmbeddingSpaceEvaluator(args, eval_net_path, lang_model, device)
                else:
                    raise ReferenceError('Could not load EmbeddingSpaceEvaluator with the old settings, check that all necesary arguments were passed to the class constructor')
            elif match_old:
                checkpoint_path = '/local_scratch2/aauterna/output/runs/saved_models/{}/train_multimodal_context/'.format(match_old[0])
                args = load_config(checkpoint_path)
                vocab_cache_path = '/local_scratch2/aauterna/ted_dataset/vocab_cache.pkl'
                lang_model = build_vocab('words', [], vocab_cache_path, args.wordembed_path, args.wordembed_dim)
                out_dim = args.pose_dim
                if args.eval_net_path and len(args.eval_net_path) > 0:
                    eval_net_path = '/local_scratch/aauterna/Documents/Work/projects/gestures_generation/code/gestures_generation/data/{}'.format(args.eval_net_path[7:])
                    embed_space_evaluator = EmbeddingSpaceEvaluator(args, eval_net_path, lang_model, device)
                else:
                    raise ReferenceError('Could not load EmbeddingSpaceEvaluator with the new settings, check that all necesary arguments were passed to the class constructor')
            else:
                raise ValueError('Path where the model was saved did not exist')
            
            rows = []
            columns = rows
            cellText = []
            compare_dir_vec = {}
            compare_dir_vec['gt_dir_vec'] = data['gt']['gt_dir_vec'].reshape(-1, args.n_poses, out_dim)
            compare_dir_vec['gt_retargeted_dir_vec'] = data['gt']['gt_retargeted_dir_vec'].reshape(-1, args.n_poses, out_dim)
            compare_dir_vec['gt_robot_dir_vec'] = data['gt']['gt_robot_dir_vec'].reshape(-1, args.n_poses, out_dim)
            compare_dir_vec['gt_sim_dir_vec'] = data['gt']['gt_sim_dir_vec'].reshape(-1, args.n_poses, out_dim)
            compare_dir_vec['gen_dir_vec'] = data['gen'][gen_model]['model_dir_vec'].reshape(-1, args.n_poses, out_dim)
            compare_dir_vec['gen_retargeted_dir_vec'] = data['gen'][gen_model]['model_retargeted_dir_vec'].reshape(-1, args.n_poses, out_dim)
            compare_dir_vec['gen_robot_dir_vec'] = data['gen'][gen_model]['gen_robot_dir_vec'].reshape(-1, args.n_poses, out_dim)
            compare_dir_vec['gen_sim_dir_vec'] = data['gen'][gen_model]['gen_sim_dir_vec'].reshape(-1, args.n_poses, out_dim)
            
            rows.append(list(compare_dir_vec.keys()))
            cellText.append([['' for j in rows[-1]] for i in rows[-1]])
            n_rows = len(rows[-1])
            n_columns = len(columns[-1])

            fig_name = 'Pseudo Confusion Matrix of f_style Embedding Spaces'
            if embed_space_viz:
                figures = [plt.figure(figsize=(14, 10))]
                figures[-1].canvas.manager.set_window_title(fig_name)
                axes = []
                for ax_idx in range(n_rows*n_columns):
                    axes.append(figures[-1].add_subplot(n_rows, n_columns, ax_idx + 1))
                    if ax_idx in range(n_rows):
                        axes[-1].set_title(rows[-1][ax_idx])
                    if ax_idx % n_columns == 0:
                        axes[-1].set_ylabel(columns[-1][ax_idx//n_columns])

            for (comb1, comb2) in combinations(compare_dir_vec, 2):
                print('Comparison between {} and {} :'.format(comb1, comb2))
                idx_comb1 = rows[-1].index(comb1)
                idx_comb2 = rows[-1].index(comb2)
                comb1_t = torch.Tensor(compare_dir_vec[comb1]).cuda()
                comb2_t = torch.Tensor(compare_dir_vec[comb2]).cuda()
                mse = nn.functional.mse_loss(comb2_t, comb1_t)
                mae = nn.functional.l1_loss(comb2_t, comb1_t)
                huber_loss = nn.functional.huber_loss(comb2_t, comb1_t)
                smooth_l1_loss = nn.functional.smooth_l1_loss(comb2_t, comb1_t)
                cos_sim = torch.mean(nn.functional.cosine_similarity(comb2_t, comb1_t))
                if embed_space_evaluator:
                    embed_space_evaluator.reset()
                    embed_space_evaluator.push_samples(None, None, comb1_t, comb2_t)
                    if embed_space_evaluator.get_no_of_samples() > 0:
                        frechet_dist, feat_dist = embed_space_evaluator.get_scores()
                        if embed_space_viz:
                            real_feats, generated_feats = embed_space_evaluator.get_features_for_viz()
                            embedding = [real_feats, generated_feats] 
                            for idx, embed in enumerate(embedding):
                                axes[n_rows*idx_comb1+idx_comb2].scatter(embed[:, 0], embed[:, 1], s=2)
                                axes[n_rows*idx_comb2+idx_comb1].scatter(embed[:, 0], embed[:, 1], s=2)
                            axes[n_rows*idx_comb1+idx_comb2].set_aspect('equal', 'datalim')
                            axes[n_rows*idx_comb2+idx_comb1].set_aspect('equal', 'datalim')
                        print('frechet_dist : {:.2f}       feat_dist : {:.1f}         MSE : {:.4f}        MAE : {:.4f}      Cosine Sim : {:.4}     Hubert : {:.4f}     Smooth l1 : {:.4f} \n'.format(frechet_dist, feat_dist, mse, mae, cos_sim, huber_loss, smooth_l1_loss))
                cellText[-1][idx_comb1][idx_comb2] = 'FGD : {:.2f}\nfeat_dist : {:.1f}\nMSE : {:.4f}\nMAE : {:.4f}\nCosine Sim : {:.4}\nHubert : {:.4f}\nSmooth l1 : {:.4f}'.format(frechet_dist, feat_dist, mse, mae, cos_sim, huber_loss, smooth_l1_loss)
                cellText[-1][idx_comb2][idx_comb1] = 'FGD : {:.2f}\nfeat_dist : {:.1f}\nMSE : {:.4f}\nMAE : {:.4f}\nCosine Sim : {:.4}\nHubert : {:.4f}\nSmooth l1 : {:.4f}'.format(frechet_dist, feat_dist, mse, mae, cos_sim, huber_loss, smooth_l1_loss)
            
            if embed_space_viz:
                figures[-1].tight_layout()
                if save:                
                    for fig in figures:
                        plot_name = 'global_analysis_len_{}_model_{}_'.format(len_analysis, gen_model)
                        plot_name += fig_name.lower().replace(' ', '_')
                        plot_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/generation/{}.png'.format(plot_name)
                        fig.savefig(plot_path, dpi=600)
                else:
                    plt.show()

            compare_ang = {
                            'gen_sim': data_gen[gen_model]['gen_robot_ang'][:, 1, :],
                            'gen_robot': data_gen[gen_model]['gen_sim_ang'][:, 1, :],
                            'gen': data_gen[gen_model]['gen_sim_ang'][:, 0, :],
                            'gt_sim': data_gt['gt_sim_ang'][:, 1, :],
                            'gt_robot': data_gt['gt_robot_ang'][:, 1, :],
                            'gt': data_gt['gt_sim_ang'][:, 0, :]
                            }

            rows.append(list(compare_ang.keys()))
            cellText.append([['' for j in rows[-1]] for i in rows[-1]])

            for (comb1, comb2) in combinations(compare_ang, 2):
                print('Comparison between {} and {} :'.format(comb1, comb2))
                idx_comb1 = rows[-1].index(comb1)
                idx_comb2 = rows[-1].index(comb2)
                comb1_t = torch.Tensor(compare_ang[comb1]).cuda()
                comb2_t = torch.Tensor(compare_ang[comb2]).cuda()
                mse = nn.functional.mse_loss(comb2_t, comb1_t)
                mae = nn.functional.l1_loss(comb2_t, comb1_t)
                huber_loss = nn.functional.huber_loss(comb2_t, comb1_t)
                smooth_l1_loss = nn.functional.smooth_l1_loss(comb2_t, comb1_t)
                cos_sim = torch.mean(nn.functional.cosine_similarity(comb2_t, comb1_t))
                print('MSE : {:.4f}        MAE : {:.4f}      Cosine Sim : {:.4}      Hubert : {:.4f}     Smooth l1 : {:.4f} \n'.format(mse, mae, cos_sim, huber_loss, smooth_l1_loss))
                cellText[-1][idx_comb1][idx_comb2] = 'MSE : {:.4f}\nMAE : {:.4f}\nCosine Sim : {:.4}\nHubert : {:.4f}\nSmooth l1 : {:.4f}'.format(mse, mae, cos_sim, huber_loss, smooth_l1_loss)
                cellText[-1][idx_comb2][idx_comb1] = 'MSE : {:.4f}\nMAE : {:.4f}\nCosine Sim : {:.4}\nHubert : {:.4f}\nSmooth l1 : {:.4f}'.format(mse, mae, cos_sim, huber_loss, smooth_l1_loss)
            
            figures = [plt.figure(figsize=(14, 10)),
                       plt.figure(figsize=(10, 8))]
            fig_names = ['Confusion Matrix Directional Vectors', 'Confusion Matrix Angles']

            for fig, fig_title in zip(figures, fig_names):
                fig.canvas.manager.set_window_title(fig_title)

            axes = [figures[0].add_subplot(),
                    figures[1].add_subplot()]

            for idx, ax in enumerate(axes):
                ax.axis('off')
                ax.set_title('Pseudo Confusion Matrix')
                table = ax.table(
                    cellText=cellText[idx], 
                    rowLabels=rows[idx],
                    colLabels=columns[idx],
                    loc='center')
                table.auto_set_column_width(col=list(range(len(columns))))
                table.scale(1, 5)
            
            for fig in figures:
                fig.tight_layout()

            if save:                
                for fig_name, fig in zip(fig_names,figures):
                    plot_name = 'global_analysis_len_{}_model_{}_'.format(len_analysis, gen_model)
                    plot_name += fig_name.lower().replace(' ', '_')
                    plot_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/generation/{}.png'.format(plot_name)
                    fig.savefig(plot_path, dpi=600)
            else:
                plt.show()
            
        if plot_animation:
            index_save_model = (list(data_gen.keys()).index(gen_save_model))
            gen_model = list(data_gen.keys())[index_save_model]
            mask_shape = data_gt['gt_dir_vec'].shape[0]-offset
            mask = np.ones(mask_shape, dtype=bool)
            for idx in range(offset):
                idx_to_rm = np.arange(34-(idx + 1), mask_shape, 34)
                mask[idx_to_rm] = False
            if offset:
                data_plot_animation_dir_vec = [
                    np.copy(data_gt['gt_dir_vec'][:-offset][mask]),
                    np.copy(data_gt['gt_fixed_shoulders_dir_vec'][:-offset][mask]),
                    np.copy(data_gt['gt_retargeted_dir_vec'][:-offset][mask]),
                    np.copy(data_gt['gt_sim_dir_vec'][offset:][mask]),
                    np.copy(data_gt['gt_robot_dir_vec'][offset:][mask]),
                    np.copy(data_gen[gen_model]['model_dir_vec'][:-offset][mask]),
                    np.copy(data_gen[gen_model]['model_fixed_shoulders_dir_vec'][:-offset][mask]),
                    np.copy(data_gen[gen_model]['model_retargeted_dir_vec'][:-offset][mask]),
                    np.copy(data_gen[gen_model]['gen_sim_dir_vec'][offset:][mask]),
                    np.copy(data_gen[gen_model]['gen_robot_dir_vec'][offset:][mask])
                ]                
                data_plot_animation_poses = [
                    np.copy(data_gt['gt_poses'][:-offset][mask]),
                    np.copy(data_gt['gt_fixed_shoulders_poses'][:-offset][mask]),
                    np.copy(data_gt['gt_retargeted_poses'][:-offset][mask]),
                    np.copy(data_gt['gt_sim_poses'][offset:][mask]),
                    np.copy(data_gt['gt_robot_poses'][offset:][mask]),
                    np.copy(data_gen[gen_model]['model_poses'][:-offset][mask]),
                    np.copy(data_gen[gen_model]['model_fixed_shoulders_poses'][:-offset][mask]),
                    np.copy(data_gen[gen_model]['model_retargeted_poses'][:-offset][mask]),
                    np.copy(data_gen[gen_model]['gen_sim_poses'][offset:][mask]),
                    np.copy(data_gen[gen_model]['gen_robot_poses'][offset:][mask]),
                ]
            else:
                data_plot_animation_dir_vec = [
                                                data_gt['gt_dir_vec'],
                                                data_gt['gt_fixed_shoulders_dir_vec'],
                                                data_gt['gt_retargeted_dir_vec'],
                                                data_gt['gt_sim_dir_vec'],
                                                data_gt['gt_robot_dir_vec'],
                                                data_gen[gen_model]['model_dir_vec'],
                                                data_gen[gen_model]['model_fixed_shoulders_dir_vec'],
                                                data_gen[gen_model]['model_retargeted_dir_vec'],
                                                data_gen[gen_model]['gen_sim_dir_vec'],
                                                data_gen[gen_model]['gen_robot_dir_vec']
                                            ]
                data_plot_animation_poses = [
                                            data_gt['gt_poses'],
                                            data_gt['gt_fixed_shoulders_poses'],
                                            data_gt['gt_retargeted_poses'],
                                            data_gt['gt_sim_poses'],
                                            data_gt['gt_robot_poses'],
                                            data_gen[gen_model]['model_poses'],
                                            data_gen[gen_model]['model_fixed_shoulders_poses'],
                                            data_gen[gen_model]['model_retargeted_poses'],
                                            data_gen[gen_model]['gen_sim_poses'],
                                            data_gen[gen_model]['gen_robot_poses']
                                            ]
            names = ['Ground Truth',
                     'Ground Truth Fixed Shoulders',
                     'Ground Truth Retargeted',
                     'Ground Truth Sim', 
                     'Ground Truth Robot', 
                     'Model {}'.format(gen_model),
                     'Model {} Fixed Shoulders'.format(gen_model),
                     'Model {} Retargeted'.format(gen_model),
                     'Model {} Sim'.format(gen_model),
                     'Model {} Robot'.format(gen_model)]

            video_name = 'global_analysis_len_{}_model_{}_offset_{}_comparison'.format(len_analysis, gen_model, offset)
            if superimpose:
                video_name += '_superimpose'
                data_plot_animation_dir_vec = [data_plot_animation_dir_vec[-3], data_plot_animation_dir_vec[-2], data_plot_animation_dir_vec[-1]]
                data_plot_animation_poses = [data_plot_animation_poses[-3], data_plot_animation_poses[-2], data_plot_animation_poses[-1]]
                names = [names[-3], names[-2], names[-1]]
                if not plot_from_dir_vec:
                    data_plot_animation_dir_vec = None
                play_arm_gestures.display_animate(poses=data_plot_animation_poses,
                                                  names=names,
                                                  dir_vec=data_plot_animation_dir_vec,
                                                  save=save,
                                                  video_name=video_name,
                                                  azim=140,
                                                  elev=-150,
                                                  superimpose=True)
            else:
                if not plot_from_dir_vec:
                    data_plot_animation_dir_vec = None
                play_arm_gestures.display_animate(poses=data_plot_animation_poses,
                                                names=names,
                                                dir_vec=data_plot_animation_dir_vec,
                                                save=save,
                                                video_name=video_name,
                                                azim=140,
                                                elev=-150,
                                                superimpose=False)

        if plot_error:
            mask_shape = data_gen[gen_model]['gen_robot_ang'].shape[0]-offset
            mask = np.ones(mask_shape, dtype=bool)
            for idx in range(offset):
                idx_to_rm = np.arange(34-(idx + 1), mask_shape, 34)
                mask[idx_to_rm] = False
            data_plot_error = {}
            data_plot_error[gen_model] = {'Gen_sim': data_gen[gen_model]['gen_robot_ang'],
                                          'Gen_robot': data_gen[gen_model]['gen_sim_ang'],
                                          'GT_sim': data_gt['gt_sim_ang'],
                                          'GT_robot': data_gt['gt_robot_ang']}
            figures = [plt.figure(figsize=(10, 8)),
                       plt.figure(figsize=(10, 8)),
                       plt.figure(figsize=(10, 8)),
                       plt.figure(figsize=(10, 8)),
                       plt.figure(figsize=(10, 8))]
            fig_names = [
                '{} : Values'.format(left_title),
                '{} : Values'.format(right_title), 
                '{} : Error'.format(left_title), 
                '{} : Error'.format(right_title), 
                mae_title
                ]
            for fig, fig_title in zip(figures, fig_names):    
                fig.canvas.manager.set_window_title(fig_title)

            axes = [figures[0].add_subplot(2, 2, 1),
                    figures[0].add_subplot(2, 2, 2), 
                    figures[0].add_subplot(2, 2, 3),
                    figures[0].add_subplot(2, 2, 4),
                    figures[1].add_subplot(2, 2, 1),
                    figures[1].add_subplot(2, 2, 2), 
                    figures[1].add_subplot(2, 2, 3),
                    figures[1].add_subplot(2, 2, 4),
                    figures[2].add_subplot(2, 2, 1),
                    figures[2].add_subplot(2, 2, 2), 
                    figures[2].add_subplot(2, 2, 3),
                    figures[2].add_subplot(2, 2, 4),
                    figures[3].add_subplot(2, 2, 1),
                    figures[3].add_subplot(2, 2, 2), 
                    figures[3].add_subplot(2, 2, 3),
                    figures[3].add_subplot(2, 2, 4),
                    figures[-1].add_subplot(1, 1, 1)]

            for ax in axes:
                ax.clear()
                ax.set_xlabel('time step')
                ax.set_ylabel('angle (rad)')
                ax.grid(alpha=0.6)

            limit_angle_label = False
            gen_desired_label = False
            gt_desired_label = False
            color_gt_desired = 'darkblue'
            color_gen_desired = 'darkred'
            color_gen_sim = 'orange'
            color_gen_robot = 'red'
            color_gt_sim = 'deepskyblue'
            color_gt_robot = 'blueviolet'

            for key, values in data_plot_error[gen_model].items():
                x = np.arange(values.shape[0]-offset)
                if offset:
                    desired = np.copy(values[:-offset, 0, :][mask, :])
                    measured = np.copy(values[offset:, 1, :][mask, :])
                    error = np.copy(np.abs(values[:-offset, 0, :] - values[offset:, 1, :])[mask, :])
                    x = x[:-(idx_to_rm.shape[0]*offset)]
                else:
                    error = np.abs(values[:, 0, :] - values[:, 1, :])
                    desired = values[:, 0, :]
                    measured = values[:, 1, :]
                right_left_mean_average = np.mean(error, axis=1)
                
                if 'Gen_sim' in key:
                    axes[-1].step(x, right_left_mean_average, label=key, color=color_gen_sim)
                if 'Gen_robot' in key:
                    axes[-1].step(x, right_left_mean_average, label=key, color=color_gen_robot)
                if 'GT_sim' in key:
                    axes[-1].step(x, right_left_mean_average, label=key, color=color_gt_sim)
                if 'GT_robot' in key:
                    axes[-1].step(x, right_left_mean_average, label=key, color=color_gt_robot)
                
                axes[-1].set_title('Mean average error (over the 8 joints)')
                for idx, joint in enumerate(joints):
                    if 'Gen' in key:
                        if not gen_desired_label:
                            axes[idx].step(x, desired[:, idx], label='desired_Gen', color=color_gen_desired)
                            gen_desired_label = True
                        else:
                            axes[idx].step(x, desired[:, idx], color=color_gen_desired)
                        if 'sim' in key:
                            axes[idx].step(x, measured[:, idx], label='measured_'+key, color=color_gen_sim)
                            axes[idx+len(joints)].step(x, error[:, idx], label='error_'+key, color=color_gen_sim)
                        if 'robot' in key:
                            axes[idx].step(x, measured[:, idx], label='measured_'+key, color=color_gen_robot)
                            axes[idx+len(joints)].step(x, error[:, idx], label='error_'+key, color=color_gen_robot)
                    if 'GT' in key:
                        if not gt_desired_label:
                            axes[idx].step(x, desired[:, idx], label='desired_GT', color=color_gt_desired)
                            gt_desired_label = True
                        else:
                            axes[idx].step(x, desired[:, idx], color=color_gt_desired)
                        if 'sim' in key:
                            axes[idx].step(x, measured[:, idx], label='measured_'+key, color=color_gt_sim)
                            axes[idx+len(joints)].step(x, error[:, idx], label='error_'+key, color=color_gt_sim)
                        if 'robot' in key:
                            axes[idx].step(x, measured[:, idx], label='measured_'+key, color=color_gt_robot)
                            axes[idx+len(joints)].step(x, error[:, idx], label='error_'+key, color=color_gt_robot)
                    axes[idx].plot(x, [max_angle[idx]]*x.shape[0], color='silver')
                    if not limit_angle_label:
                        axes[idx].plot(x, [min_angle[idx]]*x.shape[0], color='silver', label='limit angle')
                    else:
                        axes[idx].plot(x, [min_angle[idx]]*x.shape[0], color='silver')
                    axes[idx].set_title('{} : values'.format(joint))
                    axes[idx+len(joints)].set_title('{} : error'.format(joint))
                limit_angle_label = True

            # for ax in axes:
                # ax.legend(loc="upper left")

            handles, labels = axes[0].get_legend_handles_labels()
            figures[0].legend(handles, labels, loc='upper left')
            handles, labels = axes[3].get_legend_handles_labels()
            figures[1].legend(handles, labels, loc='upper left')
            handles, labels = axes[8].get_legend_handles_labels()
            figures[2].legend(handles, labels, loc='upper left')
            handles, labels = axes[9].get_legend_handles_labels()
            figures[3].legend(handles, labels, loc='upper left')
            handles, labels = axes[-1].get_legend_handles_labels()
            figures[-1].legend(handles, labels, loc='upper left')
            for fig in figures:
                fig.tight_layout()
            if save:                
                for fig, fig_name in zip(figures, fig_names):
                    plot_name = 'global_analysis_len_{}_model_{}_'.format(len_analysis, gen_model)
                    plot_name += fig_name.lower().replace(' :', '').replace(' ', '_')
                    plot_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/generation/{}.png'.format(plot_name)
                    fig.savefig(plot_path, dpi=1200)
            else:
                plt.show()


if __name__ == '__main__':
    options = None
    if len(sys.argv) > 1:
        options = sys.argv[1:]

    main(options=options)