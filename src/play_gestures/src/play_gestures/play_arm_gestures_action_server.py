#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import rospy
from play_gestures.play_arm_gestures import PlayArmGestures
import numpy as np
from numpy.linalg import inv
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
import time
from datetime import datetime
import actionlib
from pal_interaction_msgs.msg import TtsAction, TtsFeedback, TtsResult
import torch
import torchaudio
import torchaudio.functional as F
import torch.nn.functional as Fnn
from gestures_generation.utils.train_utils import load_checkpoint_and_model
from gestures_generation.utils.synthesize_utils import load_text_encoder, generate_multimodal_context_gestures
from gestures_generation.utils.data_utils import normalize_string, normalize_string_genea_2022
import warnings
warnings.filterwarnings("ignore", message=".*The 'nopython' keyword.*")
from subprocess import Popen, PIPE
import sys
import random
import yaml
from yaml.loader import SafeLoader
# gTTS
from gtts import gTTS
from miniaudio import SampleFormat, decode
# coquiTTS
from TTS.api import TTS
# multiprocessing
from multiprocessing import Process, Lock
from multiprocessing.managers import SharedMemoryManager
from multiprocessing import active_children


class PlayArmGesturesActionServer:
    def __init__(self, action_name):
        self.action_name = action_name
        self.node_name = self.action_name + '_server'        

        rospy.loginfo("Initializing the {} node".format(self.node_name))
        rospy.init_node(self.node_name, log_level=rospy.DEBUG, anonymous=True)
        
        self.rate = rospy.get_param('~rate', 10)
        self.tts_name = rospy.get_param('~tts_name', 'palTTS')
        self.mode = rospy.get_param('~mode', 'monologue')
        self.save_delta_pos = rospy.get_param('~save_delta_pos', False)
        self.simulation = rospy.get_param('~simulation', False)
        self.safe_command = rospy.get_param('~safe_command', True)
        self.j2_offset = rospy.get_param('~j2_offset', 0)
        self.decimal_num_subdivision_threshold = rospy.get_param('~decimal_num_subdivision_threshold', 0.25)
        self.saved_model_idx = rospy.get_param('~saved_model_idx', '486_0')
        self.speaker_index = rospy.get_param('~speaker_index', -1)
        self.vid_indices = None

        if self.safe_command:
            self.left_arm_pub_topic = '/arm_left_controller/safe_command'
            self.right_arm_pub_topic = '/arm_right_controller/safe_command'
        else:
            self.left_arm_pub_topic = '/arm_left_controller/command'
            self.right_arm_pub_topic = '/arm_right_controller/command'
        self.joint_states_topic = '/joint_states'
        self.joint_states_data = None

        self.get_indexes = False
        self.arm_left_1_joint_ind = None
        self.arm_left_2_joint_ind = None
        self.arm_left_3_joint_ind = None
        self.arm_left_4_joint_ind = None
        self.arm_right_1_joint_ind = None
        self.arm_right_2_joint_ind = None
        self.arm_right_3_joint_ind = None
        self.arm_right_4_joint_ind = None
        self.joint_states_position_buffer = None

        self.english_voice = 'Rachel'
        self.french_voice = 'Alice'
        self.english_lang_id = 'en_GB'
        self.french_lang_id = 'fr_FR'
    
        os.environ['CUDA_VISIBLE_DEVICES'] = '0'
        os.environ["TOKENIZERS_PARALLELISM"] = "false"
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.resample_rate = 16000
        self.current_ws_path = os.environ['ROS_PACKAGE_PATH'].split(':')[0]
        self.current_ros_package_path = '{}/play_gestures'.format(self.current_ws_path)
        self.audio_files_path = '{}/config/audio_files'.format(self.current_ros_package_path)
        os.makedirs(self.audio_files_path, exist_ok=True)
        self.checkpoint_path = '{}/config/models/trained_models'.format(self.current_ros_package_path)
        self.sentence_encoder_path = '{}/config/models/sentence_encoder_models'.format(self.current_ros_package_path)
        self.saved_models = []
        for file in os.listdir(self.checkpoint_path):
            if file.endswith(".bin"):
                self.saved_models.append(file)

        # Initialize gestures generation model
        self.speaker_model = None
        self.args = None
        self.out_dim = None
        self.dim_3 = None  # third dimension of the input/output: x, y, z
        self.generator = None
        self.text_encoder = None
        self.tokenizer = None
        self.audio_waveform = None
        self.speech_text = None
        self.in_audio = None
        self.in_text = None
        self.dir_vec = None
        self.dir_vec_idx = None
        self.stop_dir_vec_idx = None
        self.delta_pose = None
        self.save_dir_vec = []
        self.gestures = None
        self.audio_filename = None
        self.sample_rate = None
        self.waveform = None
        self.play_arm_gestures = None
        self._publishers = []
        self._subscribers = []
        self._timers = []

        # Register handler to be called when rospy process begins shutdown
        rospy.on_shutdown(self.shutdown)
        self.to_shutdown = False

        self.init_gestures_generation_model()

        # All init functions/args
        self.play_arm_gestures = PlayArmGestures(dataset=self.dataset_format)
        self.init_model()

        # multiprocessing
        self.shared_memory_manager = None
        self.max_length_buffers = 120  # 120 seconds length max to generate
        # self.init_buffers()

        # Start all the ROS related Subscribers and publishers
        self._check_all_sensors_ready()

        self._left_arm_pos_pub = rospy.Publisher(self.left_arm_pub_topic, JointTrajectory, queue_size=10)
        self._right_arm_pos_pub = rospy.Publisher(self.right_arm_pub_topic, JointTrajectory, queue_size=10)
        self._pan_vel_pub = rospy.Publisher('/head_controller/command', JointTrajectory, queue_size=10)
        self._publishers.append(self._pan_vel_pub)        
        self._publishers.append(self._left_arm_pos_pub)
        self._publishers.append(self._right_arm_pos_pub)

        self._subscribers.append(rospy.Subscriber(self.joint_states_topic, JointState, callback=self._joint_states_callback, queue_size=1))

        arm_motion_names = ['alive_inria']  # alive_3, alive_4 for the arms only
        head_motion_names = ['nod_2']  # nod for the head only
        ari_motions_filename_ari = '/opt/pal/ferrum/share/ari_bringup/config/motions/ari_motions_inria.yaml'
        ari_motions_filename_peirasmos = '{}/config/ari_motions_inria.yaml'.format(self.current_ros_package_path)
        # cmd = ['sshpass', '-p', 'pal\r', 'scp', '-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no', 'pal@ari-10c:{}'.format(ari_motions_filename_ari), ari_motions_filename_peirasmos]
        # rospy.logdebug('{}'.format(cmd))
        # output, error = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()
        # rospy.logdebug('output: {} error: {}'.format(output, error))
        ari_motions = yaml.load(open(ari_motions_filename_peirasmos), Loader=SafeLoader)['play_motion']['motions']
        self.arm_motion_to_play = [ari_motions[motion_name] for motion_name in arm_motion_names]
        self.head_motion_to_play = [ari_motions[motion_name] for motion_name in head_motion_names]
        self.arm_motion_id = 0
        self.head_motion_id = 0
        self.start_listening_mode = 0.
        self.default_time_from_start = 13

        self._timers.append(rospy.Timer(rospy.Duration(1/self.rate), callback=self.step))
        self._a_server = actionlib.SimpleActionServer(self.action_name, TtsAction, execute_cb=self.execute_cb, auto_start=False)
        self._a_server.start()

        rospy.loginfo("{} and {} Initialization Ended".format(self.node_name, self.action_name))


    def init_buffers(self):
        # Init different locks
        # self.shared_text_lock = Lock()
        # self.shared_audio_lock = Lock()
        # self.shared_gestures_lock = Lock()

        # self.shared_memory_manager = SharedMemoryManager()
        # self.shared_memory_manager.start()

        # shared_memory = self.shared_memory_manager.SharedMemory(size=gmap.nbytes)
        # self.shared_text = np.ndarray(shape=gmap.shape,
        #                                     dtype=gmap.dtype,
        #                                     buffer=shared_memory.buf)

        # shared_memory = self.shared_memory_manager.SharedMemory(size=gmap.nbytes)
        # self.shared_audio = np.ndarray(shape=gmap.shape,
        #                                     dtype=gmap.dtype,
        #                                     buffer=shared_memory.buf)

        # gmap = np.zeros((self.max_length_buffers, self.args.n_poses - self.args.n_pre_poses, self.dim_3))
        # shared_memory = self.shared_memory_manager.SharedMemory(size=gmap.nbytes)
        # self.shared_gestures = np.ndarray(shape=gmap.shape,
        #                                     dtype=gmap.dtype,
        #                                     buffer=shared_memory.buf)
        pass


    def init_gestures_generation_model(self):
        if self.tts_name == 'gTTS':
            pass
        elif self.tts_name == 'coquiTTS':
            self.tts = TTS(model_name="tts_models/en/ljspeech/tacotron2-DDC", gpu=True)
        elif self.tts_name != 'palTTS':
            # only this 3 TTS alternatives implemented
            sys.exit(0)  # rospy.signal_shutdown not working because the __init__ is not done so not yet in the spin loop
            # rospy.signal_shutdown("This TTS alternative have not been implemented")

        # load the model
        checkpoint_path = None
        for model in self.saved_models:
            if self.saved_model_idx in model:
                checkpoint_path  = '{}/{}'.format(self.checkpoint_path, model)
        if not checkpoint_path:
            rospy.logerr('Index model {} not in the saved models :{}'.format(self.saved_model_idx, self.saved_models))
            sys.exit(0)  # rospy.signal_shutdown not working because the __init__ is not done so not yet in the spin loop 
            # rospy.signal_shutdown('Shutting down')

        self.args, self.generator, discriminator, loss_fn, lang_model, self.speaker_model, self.out_dim, _ = load_checkpoint_and_model(
        checkpoint_path=checkpoint_path, 
        _device=self.device)
        self.generator.train(False)

        # load text encoder and tokenizer if any
        self.args.tensorflow_hub_models_path = '{}/tensorflow_hub_models/'.format(self.sentence_encoder_path)
        self.args.hugging_face_models_path = '{}/hugging_face_models/'.format(self.sentence_encoder_path)
        encoder = load_text_encoder(args=self.args)
        self.text_encoder = encoder[0]
        if len(encoder) > 1:
            self.tokenizer = encoder[1]

        self.unit_time = self.args.n_poses / self.args.motion_resampling_framerate
        self.stride_time = (self.args.n_poses - self.args.n_pre_poses) / self.args.motion_resampling_framerate
        self.audio_sample_length = int(self.unit_time * self.resample_rate)
        self.mean_dir_vec = np.array(self.args.mean_dir_vec_train).squeeze()
        self.p = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]])  # rotation matrix from deep model to robot joint format
        if 'ted_dataset' in self.args.train_data_path:
            self.dataset_format = 'ted'
        if 'genea_2022_dataset' in self.args.train_data_path:
            self.dataset_format = 'genea_2022'


    def init_model(self):
        # Create a buffer of n_pre_poses length to generate motion from the last n_pre_poses of the robot joints
        self.joint_states_position_buffer = np.zeros((self.args.n_pre_poses, self.play_arm_gestures.n_limbs * 3))

        self.dim_3 = int(self.out_dim/self.play_arm_gestures.n_limbs)

        # First inference to init the network
        rospy.loginfo('Initializing the trained model')
        if self.dataset_format == 'ted':
            self.speech_text = 'Today is better than Monday'
            # self.audio_filename = '{}/out_init_network_{}.wav'.format(self.audio_files_path, 'ted')
            self.sample_rate = int(np.load('{}/out_init_network_{}_sample_rate.npy'.format(self.audio_files_path, 'ted')))
            self.waveform = torch.load('{}/out_init_network_{}.pt'.format(self.audio_files_path, 'ted'))
        if self.dataset_format == 'genea_2022':
            self.speech_text = 'I love video games and humanoid robots'
            # self.audio_filename = '{}/out_init_network_{}.wav'.format(self.audio_files_path, 'genea_2022')
            self.sample_rate = int(np.load('{}/out_init_network_{}_sample_rate.npy'.format(self.audio_files_path, 'genea_2022')))
            self.waveform = torch.load('{}/out_init_network_{}.pt'.format(self.audio_files_path, 'genea_2022'))
        self.generate_gestures()
        self.reset()


    def reset(self):
        cmd = ['sshpass', '-p', 'pal\r', 'ssh', '-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no', 'pal@ari-10c', '--']
        content = 'killall aplay'  # >> /dev/null 2>&1'
        cmd.append(content)
        rospy.logdebug('{}'.format(cmd))
        # Popen(cmd, stdin=None, stdout=None, stderr=None, close_fds=True)  # w/o communicate() to not loose time to wait the logs
        output, error = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()
        rospy.logdebug('output: {} error: {}'.format(output, error))

        if self.mode == 'monologue':
            self.reset_arm_position(joints=[0.]*self.play_arm_gestures.n_joints, reset_time=2., time_from_start=2.)
        self.gestures = None
        self.dir_vec = None
        self.speech_text = None
        self.audio_filename = None
        self.sample_rate = None
        self.dir_vec_idx = None
        self.stop_dir_vec_idx = None


    def prepare_inputs(self):
        # speaker id preparation
        random.seed()
        if self.speaker_index == -1:
            self.speaker_index = random.sample(range(0, self.speaker_model.n_words), 1)[0]  # or choose manually an id that inside the learn ids
            if self.args.z_type == 'speaker':
                if not self.speaker_index:
                    self.speaker_index = random.randrange(self.generator.z_obj.n_words)
                speaker_index = torch.LongTensor([self.speaker_index])
                self.vid_indices = Fnn.one_hot(speaker_index, num_classes=self.speaker_model.n_words).to(torch.float32).to(self.device)
            else:
                self.vid_indices = None
        else:
            speaker_index = torch.LongTensor([self.speaker_index])
            self.vid_indices = Fnn.one_hot(speaker_index, num_classes=self.speaker_model.n_words).to(torch.float32).to(self.device)
        rospy.loginfo('speaker_index: {}'.format(self.speaker_index))

        # audio preparation
        if self.tts_name == 'palTTS':
            if self.audio_filename is not None:
                self.waveform, self.sample_rate = torchaudio.load(self.audio_filename)
        waveform_mono = torch.mean(self.waveform, dim=0).unsqueeze(0)
        if self.sample_rate != self.resample_rate:
            rospy.logwarn('resampling from {0} Hz to {1} Hz'.format(self.sample_rate, self.resample_rate))
            resampling_method = 'sinc_interpolation'  # kaiser_window
            waveform_mono = F.resample(waveform_mono,
                                       self.sample_rate,
                                       self.resample_rate,
                                       resampling_method=resampling_method
                                    )
        audio = waveform_mono.squeeze().numpy()

        clip_length = waveform_mono.shape[1] / self.resample_rate
        rospy.loginfo('clip length to generate: {} seconds'.format(clip_length))
        # divide into synthesize units and do synthesize
        if clip_length < self.unit_time:
            num_subdivision = 1
            decimal_num_subdivision = clip_length/self.unit_time
        else:
            # num_subdivision = np.ceil((clip_length - self.unit_time) / self.stride_time) + 1
            num_subdivision = (clip_length - self.unit_time) / self.stride_time
            decimal_num_subdivision = num_subdivision % 1
            num_subdivision = int(num_subdivision) + 2  # +1 for the unit_time and +1 for the non complete subdivision
        if decimal_num_subdivision > self.decimal_num_subdivision_threshold:
            self.stop_dir_vec_idx = np.ceil((self.args.n_poses - self.args.n_pre_poses) * (num_subdivision - 1 + decimal_num_subdivision)).astype(int)
        else:
            num_subdivision -= 1
            self.stop_dir_vec_idx = (self.args.n_poses - self.args.n_pre_poses) * num_subdivision
            if num_subdivision < 1:
                return None
        
        # pre seq prepatation
        n_frames = self.args.n_poses
        if self.args.one_prediction_auto_reg_eval:
            pre_seq = torch.zeros((1, self.args.n_pre_poses, len(self.args.mean_dir_vec_train)))
            pre_seq[:, 0:self.args.n_pre_poses, :-1] = torch.from_numpy(self.joint_states_position_buffer)
        elif self.args.one_prediction_one_shot_eval:
            pre_seq = torch.zeros((1, n_frames - 1, len(self.args.mean_dir_vec_train) + 1))
            nb_zeros_row = sum(self.joint_states_position_buffer.any(axis=1))
            pre_seq[:, 0:self.args.n_pre_poses, :-1] = torch.from_numpy(self.joint_states_position_buffer)
            # given how the self.joint_states_position_buffer array is filled, in the ascending order, bit constraints from 0 to self.args.n_pre_poses
            pre_seq[:, 0:nb_zeros_row, -1] = 1  # indicating bit for constraints
        elif self.args.one_prediction_one_prediction_eval:
            pre_seq = torch.zeros((1, n_frames - 1, len(self.args.mean_dir_vec_train)))
            # pre_seq[0, :, :] = out_dir_vec.squeeze(0)[1:] not available in this mode
        else:
            pre_seq = torch.zeros((1, n_frames, len(self.args.mean_dir_vec_train) + 1))
            nb_zeros_row = sum(self.joint_states_position_buffer.any(axis=1))
            pre_seq[:, 0:self.args.n_pre_poses, :-1] = torch.from_numpy(self.joint_states_position_buffer)
            # given how the self.joint_states_position_buffer array is filled, in the ascending order, bit constraints from 0 to self.args.n_pre_poses
            pre_seq[:, 0:nb_zeros_row, -1] = 1  # indicating bit for constraints

        # text preparation
        # TODO; if not genea_2022 dataset and to check the division
        if self.dataset_format == 'genea_2022':
            speech_text = normalize_string_genea_2022(self.speech_text)
        if self.dataset_format == 'ted':
            speech_text = normalize_string(self.speech_text)
        rospy.loginfo('text before: {}'.format(self.speech_text))
        rospy.loginfo('text after: {}'.format(speech_text))
        text_split = speech_text.split(' ')
        nb_word_by_sample = np.ceil(len(text_split) / num_subdivision).astype(int)
        text = []
        for i in range(num_subdivision):
            text.append(' '.join(text_split[i*nb_word_by_sample:(i+1)*nb_word_by_sample]))
        if (i+1)*nb_word_by_sample < len(text_split):
            text.append(' '.join(text_split[(i+1)*nb_word_by_sample:]))

        return num_subdivision, clip_length, pre_seq, waveform_mono, text


    def generate_gestures(self):
        inputs_data = self.prepare_inputs()
        if inputs_data is None:
            return None
        else:
            num_subdivision, clip_length, pre_seq, waveform_mono, text = inputs_data
        n_frames = self.args.n_poses
        out_list = []

        for i in range(num_subdivision):
            loop_time = time.time()
            start_time = i * self.stride_time

            # audio preparation
            audio_start = np.floor(start_time / clip_length * waveform_mono.shape[1]).astype(int)
            audio_end = audio_start + self.audio_sample_length
            in_audio = waveform_mono[:, audio_start:audio_end]
            # print('audio shape: {}, start: {}, end: {}'.format(waveform_mono.shape, audio_start, audio_end))
            if in_audio.shape[1] < self.audio_sample_length:
                in_audio = Fnn.pad(in_audio, (0, self.audio_sample_length - in_audio.shape[1]), 'constant')                
            
            # text preparation
            in_text = [text[i]]

            # prepare pre seq
            if i > 0:
                if self.args.one_prediction_auto_reg_eval:
                    pre_seq = torch.zeros((1, self.args.n_pre_poses, len(self.args.mean_dir_vec_train)))
                    pre_seq[0, :, :-1] = out_dir_vec.squeeze(0)[-self.args.n_pre_poses:]
                elif self.args.one_prediction_one_shot_eval:
                    pre_seq = torch.zeros((1, n_frames - 1, len(self.args.mean_dir_vec_train) + 1))
                    pre_seq[0, 0:self.args.n_pre_poses, :-1] = out_dir_vec.squeeze(0)[-self.args.n_pre_poses:]
                    pre_seq[0, 0:self.args.n_pre_poses, -1] = 1  # indicating bit for constraints
                elif self.args.one_prediction_one_prediction_eval:
                    pre_seq = torch.zeros((1, n_frames - 1, len(self.args.mean_dir_vec_train)))
                    pre_seq[0, :, :] = out_dir_vec.squeeze(0)[1:]
                else:
                    pre_seq = torch.zeros((1, n_frames, len(self.args.mean_dir_vec_train) + 1))
                    pre_seq[:, 0:self.args.n_pre_poses, :-1] = out_dir_vec.squeeze(0)[-self.args.n_pre_poses:]
                    pre_seq[:, 0:self.args.n_pre_poses, -1] = 1  # indicating bit for constraints
    
            if self.args.model == 'multimodal_context':
                out_dir_vec, out_seq = generate_multimodal_context_gestures(
                    args=self.args,
                    generator=self.generator,
                    data=(pre_seq, in_text, in_audio, self.vid_indices),
                    text_encoder=self.text_encoder,
                    device=self.device,
                    tokenizer=self.tokenizer)
            # smoothing motion transition
            if len(out_list) > 0:
                last_poses = out_list[-1][-self.args.n_pre_poses:]
                out_list[-1] = out_list[-1][:-self.args.n_pre_poses]  # delete last 4 frames
                for j in range(len(last_poses)):
                    n = len(last_poses)
                    prev = last_poses[j]
                    next = out_seq[j]
                    out_seq[j] = prev * (n - j) / (n + 1) + next * (j + 1) / (n + 1)
            out_list.append(out_seq)
            print('Inference time deep model {}'.format(time.time() - loop_time))

            # send out_seq to displayed on the robot
            self.prepare_gestures_to_robot(gestures=out_seq[:-self.args.n_pre_poses])
        
        # aggregate results
        out_list[-1] = out_list[-1][self.args.n_pre_poses:]  # delete first 4 frames because first 4 frames from seed
        out_dir_vec = np.vstack(out_list)


    def prepare_gestures_to_robot(self, gestures):
        if self.save_delta_pos:
            if self.gestures is None:
                self.gestures = gestures
            else:
                self.gestures = np.vstack((self.gestures, gestures))
        out_dir_vec = gestures + self.mean_dir_vec
        out_dir_vec = out_dir_vec.reshape(-1, self.play_arm_gestures.n_limbs, 3)
        out_dir_vec[:, :, 0] = -out_dir_vec[:, :, 0]
        out_dir_vec = np.matmul(out_dir_vec, self.p)
        
        if self.dir_vec is None:
            self.dir_vec = out_dir_vec
            self.dir_vec_idx = 0
        else:
            self.dir_vec = np.vstack((self.dir_vec, out_dir_vec))
            # add Lock
            # self.dir_vec = self.dir_vec[self.dir_vec_idx, :, :]


    def execute_cb(self, goal):
        rate = rospy.Rate(self.rate)
        self.init_time = rospy.Time.now()
        self._result = TtsResult()
        self._feedback = TtsFeedback()
        self.speech_text = goal.rawtext.text
        language, voice = self.get_language(goal.rawtext.lang_id)
        tts_init_time = time.time()
        
        if self.tts_name == 'palTTS':
            # cmd = 'sshpass -p pal ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" pal@ari-10c -- "source /usr/bin/init_pal_env.sh >> /dev/null 2>&1 && /opt/pal/ferrum/lib/pal_tts/say_to_file.sh {0} {1} {2} >> /dev/null 2>&1" >> /dev/null 2>&1'.format(
            #     language,
            #     voice,
            #     self.speech_text
            # )
            # os.system(cmd)
            cmd = ['sshpass', '-p', 'pal\r', 'ssh', '-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no', 'pal@ari-10c', '--']
            content = 'source /usr/bin/init_pal_env.sh && /opt/pal/ferrum/lib/pal_tts/say_to_file.sh {0} {1} {2}'.format(
                language,
                voice,
                self.speech_text
            )
            cmd.append(content)
            rospy.logdebug('{}'.format(cmd))
            output, error = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()
            rospy.logdebug('output: {} error: {}'.format(output, error))
            print('time tts ssh ', time.time() - tts_init_time)
            tts_init_time = time.time()
            dt_string = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
            self.audio_filename = '{}/out_{}.wav'.format(self.audio_files_path, dt_string)
            # cmd = 'sshpass -p pal scp pal@ari-10c:/home/pal/out.wav {}/out_{}.wav'.format(self.audio_files_path, dt_string)
            # os.system(cmd)
            cmd = ['sshpass', '-p', 'pal\r', 'scp', '-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no', 'pal@ari-10c:/home/pal/out.wav', self.audio_filename]
            rospy.logdebug('{}'.format(cmd))
            output, error = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()
            rospy.logdebug('output: {} error: {}'.format(output, error))
            print('time tts scp ', time.time() - tts_init_time)

        if self.tts_name == 'gTTS':
            myobj = gTTS(text=self.speech_text, lang=language, slow=False, tld=voice)
            audio = myobj.stream()
            audio_t = []
            waveform_flag = False
            for idx, decoded in enumerate(audio):
                audio_t.append(decoded)
                decoded_audio = decode(audio_t[idx], nchannels=1, sample_rate=self.resample_rate, output_format=SampleFormat.SIGNED16)
                decoded_audio = torch.FloatTensor(decoded_audio.samples)
                decoded_audio /= (1 << 15)
                if not waveform_flag:
                    self.waveform = decoded_audio
                    waveform_flag = True
                else:
                    self.waveform = torch.cat((self.waveform, decoded_audio), 0)
            self.waveform = self.waveform.unsqueeze(0)
            self.sample_rate = self.resample_rate
            torchaudio.save('{}/out.wav'.format(self.audio_files_path), self.waveform, sample_rate=self.resample_rate)
            print('time gTTS ', time.time() - tts_init_time)
            cmd = cmd = ['sshpass', '-p', 'pal\r', 'scp', '-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no', '{}/out.wav'.format(self.audio_files_path), 'pal@ari-10c:/home/pal/out.wav']
            rospy.logdebug('{}'.format(cmd))
            Popen(cmd, stdin=None, stdout=None, stderr=None, close_fds=True)
        
        if self.tts_name == 'coquiTTS':
            self.waveform = self.tts.tts(self.speech_text)
            self.waveform = torch.FloatTensor(self.waveform)
            self.waveform = F.resample(waveform=self.waveform, orig_freq=22050, new_freq=self.resample_rate, resampling_method='sinc_interpolation')
            self.waveform = self.waveform.unsqueeze(0)
            self.sample_rate = self.resample_rate
            torchaudio.save('{}/out.wav'.format(self.audio_files_path), self.waveform, sample_rate=self.resample_rate)
            print('time coquiTTS ', time.time() - tts_init_time)
            cmd = cmd = ['sshpass', '-p', 'pal\r', 'scp', '-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no', '{}/out.wav'.format(self.audio_files_path), 'pal@ari-10c:/home/pal/out.wav']
            rospy.logdebug('{}'.format(cmd))
            Popen(cmd, stdin=None, stdout=None, stderr=None, close_fds=True)

        self.generate_gestures()

        """ sometimes the scp command takes more time than the gesture generation and the audio is removed when sending.
        This cause an issue, we want to read an audio file that has not been sent. We have time to remove it but no time
        between its creation and the gesture generation
        """
        time.sleep(1)

        if self.tts_name == 'palTTS':
            cmd = ['rm', self.audio_filename]
            rospy.logdebug('{}'.format(cmd))
            Popen(cmd, stdin=None, stdout=None, stderr=None, close_fds=True)

        if self.tts_name == 'gTTS' or self.tts_name == 'coquiTTS':
            cmd = ['rm', '{}/out.wav'.format(self.audio_files_path)]
            rospy.logdebug('{}'.format(cmd))
            Popen(cmd, stdin=None, stdout=None, stderr=None, close_fds=True)

        # start executing the action
        # while not rospy.is_shutdown():
        #     if self._a_server.is_preempt_requested():
        #         self._a_server.set_preempted()
        #         break
            
        #     self.current_time = (rospy.Time.now() - self.init_time).to_sec()
        #     # create messages that are used to publish feedback/result
        #     self._feedback = TtsFeedback()

        #     # publish the feedback
        #     self._publish_feedback(goal)
        #     rate.sleep()

        #     if self.current_time > 2:
        #         rospy.loginfo('%s: Succeeded' % self.action_name)
        #         break
        # TODO: can do a rospy Timer to publish feedbacks regularly
        
        self._result_msg(goal)

    
    def get_language(self, lang_id):
        if lang_id == self.english_lang_id:
            language = 'english'
            voice = self.english_voice
            if self.tts_name == 'gTTS':
                language = 'en'
                voice = 'com.au'  # or only com
        elif lang_id == self.french_lang_id:
            language = 'french'
            voice = self.french_voice
            if self.tts_name == 'gTTS':
                language = 'fr'
                voice = 'fr'  # or only com
        else:
            rospy.logerr('Could not get language and voice because only langugae {} with {} voice and language {} with {} voice are implemented'.format(
                self.english_lang_id,
                self.english_voice,
                self.french_lang_id,
                self.french_voice
                ))
        return language, voice


    def _publish_feedback(self, goal):
        self._feedback.text_said = goal.rawtext.text
        # self._feedback.event_type = 32
        self._a_server.publish_feedback(self._feedback)


    def _result_msg(self, goal):
        self._result.text = goal.rawtext.text
        self._a_server.set_succeeded(self._result)


    def step(self, event):
        # TODO: play audio sample on ARI corresponding to x dir_vec sample
        
        if self.dir_vec is not None:
            if self.dir_vec_idx == 0:
                # cmd = 'sshpass -p pal ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" pal@ari-10c -- "timeout {} play {}{} 2> /dev/null " &'.format(self.timeout_ckpt-2, 'ted_gen_2_vid_730', self.audio_extension)
                # rospy.loginfo(cmd)
                # os.system(cmd)
                cmd = ['sshpass', '-p', 'pal\r', 'ssh', '-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no', 'pal@ari-10c', '--']
                content = 'aplay out.wav >> /dev/null 2>&1'
                cmd.append(content)
                rospy.logdebug('{}'.format(cmd))
                Popen(cmd, stdin=None, stdout=None, stderr=None, close_fds=True)  # w/o communicate() to not loose time to wait the logs
                rospy.sleep(1.5)
                # rospy.logdebug('output: {} error: {}'.format(output, error))
            if self.dir_vec_idx != self.stop_dir_vec_idx:
                if self.dataset_format == 'ted':
                    dir_vec = np.array([[0., 0., 0.],
                                        self.dir_vec[self.dir_vec_idx][-2],
                                        self.dir_vec[self.dir_vec_idx][-1],
                                        self.dir_vec[self.dir_vec_idx][-5],
                                        self.dir_vec[self.dir_vec_idx][-4]])
                if self.dataset_format == 'genea_2022':
                    dir_vec = np.array([[0., 0., 0.],
                                        self.dir_vec[self.dir_vec_idx][-4],
                                        self.dir_vec[self.dir_vec_idx][-3],
                                        self.dir_vec[self.dir_vec_idx][2],
                                        self.dir_vec[self.dir_vec_idx][3]])
                joints = self.play_arm_gestures.compute_angle_pos_from_dir_vec(dir_vec=dir_vec)
                if self.j2_offset:
                    joints[1] += np.radians(self.j2_offset)
                    joints[5] += np.radians(self.j2_offset)
                joints = np.round(joints, decimals=5)
                if self.save_delta_pos:
                    self.save_dir_vec.append(dir_vec)
                limit, joints = self.play_arm_gestures.check_joint_limits(joints=joints)
                if limit:           
                        # time.sleep(1)
                        rospy.logerr("Joint limit reached in one or more joint")
                        # rospy.signal_shutdown("Joint limit reached in one or more joint")
                self.publish_arm_gestures(joints, time_from_start=0.2)
                self.compute_delta_pos(joints)
                self.dir_vec_idx += 1
            else:
                self.reset()
        else:
            if self.start_listening_mode == 0. or (time.time() - self.start_listening_mode) >= self.default_time_from_start:
                self.start_listening_mode = time.time()
                arm_gestures = np.asarray([motion_points['positions'][:self.play_arm_gestures.n_joints] for  motion_points in self.arm_motion_to_play[self.arm_motion_id]['points']])
                time_from_start = [motion_points['time_from_start'] for motion_points in self.arm_motion_to_play[self.arm_motion_id]['points']]
                self.publish_arm_gestures(arm_gestures, time_from_start=time_from_start)
                self.arm_motion_id += 1
                if self.arm_motion_id >= len(self.arm_motion_to_play):
                    self.arm_motion_id = 0

                head_gestures = np.asarray([motion_points['positions'] for  motion_points in self.head_motion_to_play[self.head_motion_id]['points']])
                time_from_start = [motion_points['time_from_start'] for motion_points in self.head_motion_to_play[self.head_motion_id]['points']]
                self.publish_head_gestures(head_gestures, time_from_start=time_from_start)
                self.head_motion_id += 1
                if self.head_motion_id >= len(self.head_motion_to_play):
                    self.head_motion_id = 0

        if self.dir_vec_idx:
            rospy.logdebug('dir_vec_idx: {}'.format(self.dir_vec_idx))


    def reset_arm_position(self, joints, reset_time=2., time_from_start=2.):
        tt = time.time()
        while time.time() - tt < reset_time:
            self.publish_arm_gestures(joints, time_from_start=time_from_start)


    def publish_head_gestures(self, head_gestures, time_from_start=0.1):       
        head_msg = JointTrajectory()
        head_msg.joint_names = [None]*2
        head_msg.joint_names[0] = "head_1_joint"  # pan
        head_msg.joint_names[1] = "head_2_joint"  # tilt
        if isinstance(head_gestures, np.ndarray):
            head_msg.points = [None] * head_gestures.shape[0]
            for i in range(head_gestures.shape[0]):
                p = JointTrajectoryPoint()
                p.positions = [None] * 2
                p.positions[0] = head_gestures[i, 0]
                p.positions[1] = head_gestures[i, 1]                
                head_msg.points[i] = p
                head_msg.points[i].time_from_start = rospy.Duration.from_sec(time_from_start[i])
        if isinstance(head_gestures, list):
            head_msg.points = [None]
            p = JointTrajectoryPoint()
            p.positions = [None] * 2
            p.positions[0] = head_gestures[0]
            p.positions[1] = head_gestures[1]
            head_msg.points[0] = p
            head_msg.points[0].time_from_start = rospy.Duration.from_sec(time_from_start)
        self._pan_vel_pub.publish(head_msg)


    def publish_arm_gestures(self, arm_gestures, time_from_start=0.1):
        # Left arm publication
        left_arm_gestures_msg = JointTrajectory()
        left_arm_gestures_msg.joint_names = [None]*4
        left_arm_gestures_msg.joint_names[0] = "arm_left_1_joint"
        left_arm_gestures_msg.joint_names[1] = "arm_left_2_joint"
        left_arm_gestures_msg.joint_names[2] = "arm_left_3_joint"
        left_arm_gestures_msg.joint_names[3] = "arm_left_4_joint"
        if isinstance(arm_gestures, np.ndarray):
            left_arm_gestures_msg.points = [None] * arm_gestures.shape[0]
            for i in range(arm_gestures.shape[0]):
                left_p = JointTrajectoryPoint()
                left_p.positions = [None] * 4
                left_p.positions[0] = arm_gestures[i, 0]
                left_p.positions[1] = arm_gestures[i, 1]
                left_p.positions[2] = arm_gestures[i, 2]
                left_p.positions[3] = arm_gestures[i, 3]
                left_arm_gestures_msg.points[i] = left_p
                left_arm_gestures_msg.points[i].time_from_start = rospy.Duration.from_sec(time_from_start[i])
        elif isinstance(arm_gestures, list):
            left_arm_gestures_msg.points = [None]
            left_p = JointTrajectoryPoint()
            left_p.positions = [None] * 4
            left_p.positions[0] = arm_gestures[0]
            left_p.positions[1] = arm_gestures[1]
            left_p.positions[2] = arm_gestures[2]
            left_p.positions[3] = arm_gestures[3]        
            left_arm_gestures_msg.points[0] = left_p
            left_arm_gestures_msg.points[0].time_from_start = rospy.Duration.from_sec(time_from_start)
        self._left_arm_pos_pub.publish(left_arm_gestures_msg)

        # Right arm publication
        right_arm_gestures_msg = JointTrajectory()
        right_arm_gestures_msg.joint_names = [None]*4
        right_arm_gestures_msg.joint_names[0] = "arm_right_1_joint"
        right_arm_gestures_msg.joint_names[1] = "arm_right_2_joint"
        right_arm_gestures_msg.joint_names[2] = "arm_right_3_joint"
        right_arm_gestures_msg.joint_names[3] = "arm_right_4_joint"
        if isinstance(arm_gestures, np.ndarray):
            right_arm_gestures_msg.points = [None] * arm_gestures.shape[0]
            for i in range(arm_gestures.shape[0]):
                right_p = JointTrajectoryPoint()
                right_p.positions = [None] * 4
                right_p.positions[0] = arm_gestures[i, 4]
                right_p.positions[1] = arm_gestures[i, 5]
                right_p.positions[2] = arm_gestures[i, 6]
                right_p.positions[3] = arm_gestures[i, 7]
                right_arm_gestures_msg.points[i] = right_p
                right_arm_gestures_msg.points[i].time_from_start = rospy.Duration.from_sec(time_from_start[i])
        elif isinstance(arm_gestures, list):
            right_arm_gestures_msg.points = [None]
            right_p = JointTrajectoryPoint()
            right_p.positions = [None] * 4
            right_p.positions[0] = arm_gestures[4]
            right_p.positions[1] = arm_gestures[5]
            right_p.positions[2] = arm_gestures[6]
            right_p.positions[3] = arm_gestures[7]
            right_arm_gestures_msg.points[0] = right_p
            right_arm_gestures_msg.points[0].time_from_start = rospy.Duration.from_sec(time_from_start)
        self._right_arm_pos_pub.publish(right_arm_gestures_msg)

    
    def compute_delta_pos(self, joint_targets):
        lj1 , lj2, lj3, lj4 ,rj1, rj2, rj3, rj4 = joint_targets
        lj1_v = self.joint_states_data.position[self.arm_left_1_joint_ind]
        lj2_v = self.joint_states_data.position[self.arm_left_2_joint_ind]
        lj3_v = self.joint_states_data.position[self.arm_left_3_joint_ind]
        lj4_v = self.joint_states_data.position[self.arm_left_4_joint_ind]
        rj1_v = self.joint_states_data.position[self.arm_right_1_joint_ind]
        rj2_v = self.joint_states_data.position[self.arm_right_2_joint_ind]
        rj3_v = self.joint_states_data.position[self.arm_right_3_joint_ind]
        rj4_v = self.joint_states_data.position[self.arm_right_4_joint_ind]
        delta = np.array([[lj1 - lj1_v,
                           lj2 - lj2_v,
                           lj3 - lj3_v,
                           lj4 - lj4_v],
                          [rj1 - rj1_v,
                           rj2 - rj2_v,
                           rj3 - rj3_v,
                           rj4 - rj4_v]])
        delta = abs(delta)
        # rospy.loginfo('delta : {}, mean average error : {}'.format(delta, np.mean(delta)))
        # rospy.loginfo('mean error : {} (left/right arm)'.format(np.mean(delta, axis=1)))
        rospy.loginfo('joints desired : {}'.format(joint_targets))
        if self.save_delta_pos:
            delta_pose = np.array([[lj1 , lj2, lj3, lj4 ,rj1, rj2, rj3, rj4],
                                   [lj1_v , lj2_v, lj3_v, lj4_v ,rj1_v, rj2_v, rj3_v, rj4_v]
                                  ])
            if self.delta_pose is None:
                self.delta_pose = delta_pose
            else:
                self.delta_pose = np.vstack((self.delta_pose, delta_pose))
            
        return delta


    def _joint_states_callback(self, data):
        self.joint_states_data = data
        if self.joint_states_position_buffer is not None:
            lj1 = self.joint_states_data.position[self.arm_left_1_joint_ind]
            lj2 = self.joint_states_data.position[self.arm_left_2_joint_ind]
            lj3 = self.joint_states_data.position[self.arm_left_3_joint_ind]
            lj4 = self.joint_states_data.position[self.arm_left_4_joint_ind]
            rj1 = self.joint_states_data.position[self.arm_right_1_joint_ind]
            rj2 = self.joint_states_data.position[self.arm_right_2_joint_ind]
            rj3 = self.joint_states_data.position[self.arm_right_3_joint_ind]
            rj4 = self.joint_states_data.position[self.arm_right_4_joint_ind]
            dir_vec_extended = self.play_arm_gestures.compute_dir_vec_from_angle_pos(joints=[lj1, lj2, lj3, lj4, rj1, rj2, rj3, rj4])
            pre_poses = np.matmul(dir_vec_extended, inv(self.p))
            pre_poses[:, 0] = -pre_poses[:, 0]
            pre_poses = pre_poses.flatten()
            pre_poses -= self.mean_dir_vec
            self.joint_states_position_buffer[:-1, :] = self.joint_states_position_buffer[1:, :]
            self.joint_states_position_buffer[-1, -6:] = pre_poses[-6:]  # -6 : two last dir_vec of dim 3 nLSE-nLEH
            self.joint_states_position_buffer[-1, -15:-9] = pre_poses[-15:-9]  # -15:-9 : indices of dir_vec of dim 3 nRSE-nREH


    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_joint_states_ready()
        rospy.logdebug("ALL SENSORS READY")


    def _check_joint_states_ready(self):
        self.joint_states_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.joint_states_topic))
        while self.joint_states_data is None and not rospy.is_shutdown():
            try:
                self.joint_states_data = rospy.wait_for_message(self.joint_states_topic, JointState, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.joint_states_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for getting joint states".format(self.joint_states_topic))
        if not self.get_indexes:
            for index, name in enumerate(self.joint_states_data.name):
                if name == "arm_left_1_joint":
                    self.arm_left_1_joint_ind = index
                if name == "arm_left_2_joint":
                    self.arm_left_2_joint_ind = index
                if name == "arm_left_3_joint":
                    self.arm_left_3_joint_ind = index
                if name == "arm_left_4_joint":
                    self.arm_left_4_joint_ind = index
                if name == "arm_right_1_joint":
                    self.arm_right_1_joint_ind = index
                if name == "arm_right_2_joint":
                    self.arm_right_2_joint_ind = index
                if name == "arm_right_3_joint":
                    self.arm_right_3_joint_ind = index
                if name == "arm_right_4_joint":
                    self.arm_right_4_joint_ind = index
            self.get_indexes = True
        return self.joint_states_data


    def shutdown(self):
        rospy.loginfo("Stopping the {} node and action server {}".format(self.node_name, self.action_name))
        self.to_shutdown = True

        if self.save_delta_pos:
            self.save_data()                

        # cmd = 'sshpass -p pal ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" pal@ari-10c -- "killall play"'
        # rospy.loginfo(cmd)
        # os.system(cmd)
        cmd = ['sshpass', '-p', 'pal\r', 'ssh', '-o UserKnownHostsFile=/dev/null', '-o StrictHostKeyChecking=no', 'pal@ari-10c', '--']
        content = 'killall aplay'
        cmd.append(content)
        rospy.logdebug('{}'.format(cmd))
        output, error = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()
        rospy.logdebug('output: {} error: {}'.format(output, error))

         # Close evething
        if self.play_arm_gestures:
            self.reset_arm_position(joints=[0.]*self.play_arm_gestures.n_joints, reset_time=2., time_from_start=2.)

        self.close()
        rospy.loginfo("Killing the {} node and action server {}".format(self.node_name, self.action_name))


    def save_data(self):
        # Saving data
        dt_string = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
        save_path = '/home/ros/play_gestures_ws/src/play_gestures/config/data/'
        # if self.gesture_style == 'test_dataset':
        #     filename_np = 'len_{}_date_{}'.format(self.out_dir_vec.shape[0], dt_string)
        #     if self.simulation:
        #         filename_np += '_sim'
        #     if self.safe_command:
        #         filename_np += '_safe_cmd'
        #     if self.j2_offset != 0.:
        #         if self.j2_offset < 0.:
        #             filename_np += '_j2_offset_m{}'.format(np.abs(self.j2_offset))
        #         if self.j2_offset > 0.:
        #             filename_np += '_j2_offset_{}'.format(self.j2_offset)
        #     path_np = '{}{}.npy'.format(self.save_path, filename_np)
        # elif self.gesture_style == 'generated_test_dataset':
        #     filename_np = 'ckpt_{}_len_{}_date_{}'.format(self.ckpt_date, self.out_dir_vec.shape[0], dt_string)
        #     if self.simulation:
        #         filename_np += '_sim'
        #     if self.safe_command:
        #         filename_np += '_safe_cmd'
        #     if self.j2_offset != 0.:
        #         if self.j2_offset < 0.:
        #             filename_np += '_j2_offset_m{}'.format(np.abs(self.j2_offset))
        #         if self.j2_offset > 0.:
        #             filename_np += '_j2_offset_{}'.format(self.j2_offset)
        #     path_np = '{}{}.npy'.format(self.save_path, filename_np)
        # elif self.gesture_style == 'mean_pose':
        #     filename_np = 'mean_pose_len_{}_date_{}'.format(self.max_time_sec, dt_string)
        #     if self.simulation:
        #         filename_np += '_sim'
        #     if self.safe_command:
        #         filename_np += '_safe_cmd'
        #     path_np = '{}{}.npy'.format(self.save_path, filename_np)
        # else:
        #     filename_np = '{}_delta_pose_{}.npy'.format(self.checkpoint_name, dt_string)
        #     if self.simulation:
        #         filename_np += '_sim'
        #     if self.safe_command:
        #         filename_np += '_safe_cmd'
        #     path_np = '{}{}.npy'.format(self.local_path, filename_np)
        #     np_data = self.delta_pose[:self.pose_idx - 2]
        j2_offset = '{}'.format(abs(self.j2_offset))
        if self.j2_offset < 0:
            j2_offset = 'm{}'.format(abs(self.j2_offset))
        filename_np = 'joints_pose_{}_model_{}_j2_{}_speaker_{}'.format(dt_string, self.saved_model_idx, j2_offset, self.speaker_index[0])
        path_np = '{}{}.npy'.format(save_path, filename_np)
        np.save(open(path_np, 'wb'), self.delta_pose)
        filename_np = 'dir_vec_{}_model_{}_j2_{}_speaker_{}'.format(dt_string, self.saved_model_idx, j2_offset, self.speaker_index[0])
        path_np = '{}{}.npy'.format(save_path, filename_np)
        np.save(open(path_np, 'wb'), self.save_dir_vec)
        filename_np = 'gestures_{}_model_{}_j2_{}_speaker_{}'.format(dt_string, self.saved_model_idx, j2_offset, self.speaker_index[0])
        path_np = '{}{}.npy'.format(save_path, filename_np)
        np.save(open(path_np, 'wb'), self.gestures)


    def close(self):
        active = active_children()
        rospy.loginfo(f'Active Children before closing: {len(active)}')

        if self._subscribers:
            for subscriber in self._subscribers:
                subscriber.unregister()
        if self._publishers:
            for publisher in self._publishers:
                if isinstance(publisher, dict):
                    for pub in publisher.values():
                        pub.unregister()
                else:
                    publisher.unregister()
        if self._timers:
            for timer in self._timers:
                timer.shutdown()

        active = active_children()
        rospy.loginfo(f'Active Children at the end of closing: {len(active)}')
