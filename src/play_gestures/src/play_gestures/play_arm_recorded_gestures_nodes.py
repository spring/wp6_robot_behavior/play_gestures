#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import rospy
from play_gestures.play_arm_gestures import PlayArmGestures
import pickle
import numpy as np
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from sensor_msgs.msg import JointState
import time
from datetime import datetime
from play_gestures.utils import power_space, exponential_space, log_space
import re
import warnings
warnings.filterwarnings("ignore", message=".*The 'nopython' keyword.*")


class PlayArmGesturesRosNode:
    def __init__(self, node_name):
        self.node_name = node_name        

        rospy.loginfo("Initializing the {} node".format(self.node_name))
        rospy.init_node(self.node_name, log_level=rospy.DEBUG, anonymous=True)
        self.rate = rospy.get_param('~rate', 10)
        self.save_delta_pos = rospy.get_param('~save_delta_pos', False)
        self.simulation = rospy.get_param('~simulation', False)
        self.gesture_style = rospy.get_param('~gesture_style', None)
        self.safe_command = rospy.get_param('~safe_command', True)
        self.j2_offset = rospy.get_param('~j2_offset', 0.)
        self.n_sequences = rospy.get_param('~n_sequences', 5)

        if self.safe_command:
            self.left_arm_pub_topic = '/arm_left_controller/safe_command'
            self.right_arm_pub_topic = '/arm_right_controller/safe_command'
        else:
            self.left_arm_pub_topic = '/arm_left_controller/command'
            self.right_arm_pub_topic = '/arm_right_controller/command'
        self.joint_states_topic = '/joint_states'
        self.joint_states_data = None

        self.sign = 1

        self.get_indexes = False
        self.arm_left_1_joint_ind = None
        self.arm_left_2_joint_ind = None
        self.arm_left_3_joint_ind = None
        self.arm_left_4_joint_ind = None
        self.arm_right_1_joint_ind = None
        self.arm_right_2_joint_ind = None
        self.arm_right_3_joint_ind = None
        self.arm_right_4_joint_ind = None

        # Start all the ROS related Subscribers and publishers
        self._check_all_sensors_ready()

        self._left_arm_pos_pub = rospy.Publisher(self.left_arm_pub_topic, JointTrajectory, queue_size=10)
        self._right_arm_pos_pub = rospy.Publisher(self.right_arm_pub_topic, JointTrajectory, queue_size=10)

        rospy.Subscriber(self.joint_states_topic, JointState, callback=self._joint_states_callback, queue_size=1)

        # Register handler to be called when rospy process begins shutdown
        rospy.on_shutdown(self.shutdown)
        self.to_shutdown = False

        # All init functions/args
        self.play_arm_gestures = PlayArmGestures(dataset='genea_2022')
        self.init_pose()

        rospy.Timer(rospy.Duration(1/self.rate), callback=self.step)

        rospy.loginfo("{} Initialization Ended".format(self.node_name))


    def init_pose(self):
        self.reset_arm_position([0.]*self.play_arm_gestures.n_joints, reset_time=5., time_from_start=1.)
        self.pose_idx = 0
        if self.gesture_style == 'np_data_saved':
            self.local_path = '/home/ros/ari_public_ws/src/play_gestures/config/data/'
            self.dir_vec_saved = np.load(open('{}gestures_{}.npy'.format(self.local_path, '22_06_2023_17_42_22'), 'rb'))
            mean_dir_vec = np.array([-0.01660, -0.99101, -0.09250, -0.96635, -0.09992, 0.14040, -0.18765, 0.94844, 0.10040, -0.00691, 0.60245, 0.49706, 0.95714, -0.12427, 0.15668, 0.15887, 0.94644, 0.15432, 0.06968, 0.59371, 0.47072, -0.01881, -0.96075, 0.24641, -0.00252, -0.84802, 0.47761])  # genea_2022
            mean_pose = np.array([0.00000, 0.00000, 0.00000, -0.00614, -0.36623, -0.03417, -0.38346, -0.40540, 0.02184, -0.47954, 0.08020, 0.07325, -0.48297, 0.37902, 0.31979, 0.36344, -0.41410, 0.02695, 0.44478, 0.07047, 0.10596, 0.47934, 0.36495, 0.33944, -0.01109, -0.61989, 0.03087, -0.01146, -0.74042, 0.09876])  # genea_2022
            self.joints_pose_saved = np.load(open('{}joints_pose_{}.npy'.format(self.local_path, '22_06_2023_17_42_22'), 'rb'))[::2, :]
            print('shape', self.dir_vec_saved.shape, self.joints_pose_saved.shape)
            # self.dir_vec_saved += mean_dir_vec
            # self.dir_vec_saved = self.dir_vec_saved.reshape(-1, 9, 3)
            # self.dir_vec_saved[:, :, 0] = -self.dir_vec_saved[:, :, 0]
            # p = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]])
            # self.dir_vec_saved = self.dir_vec_saved@ p
            # print(self.dir_vec_saved.shape)
        elif self.gesture_style == 'checkpoint':
            self.local_path = '/local_scratch2/aauterna/output/generation_results/'
            self.checkpoint_name = 'ted_gen_2_vid_490' #'ted_gen_2_vid_213'  # '1_0_39.56_trimmed_1_vid_1226' 'ted_gen_2_vid_52'  #'ted_gen_2_vid_617' 'ted_gen_2_vid_170'
            pickle_path = self.local_path + self.checkpoint_name + '.pkl'
            self.audio_extension = '_000_0.wav'
            self.timeout_ckpt = 60
            data = pickle.load(open(pickle_path, 'rb'))
            out_dir_vec = data['out_dir_vec'].reshape(-1, 9, 3)
            out_poses = data['out_poses']
            out_poses[:, :, 0] = -out_poses[:, :, 0]
            out_dir_vec[:, :, 0] = -out_dir_vec[:, :, 0]
            p = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]])
            self.new_out_poses = np.dot(out_poses, p)  # out_poses @ p
            self.new_out_dir_vec = np.dot(out_dir_vec, p)  # out_dir_vec @ p
            mean_dir_vec = np.array([ 0.0154009, -0.9690125, -0.0884354, -0.0022264, -0.8655276, 0.4342174, -0.0035145, -0.8755367, -0.4121039, -0.9236511, 0.3061306, -0.0012415, -0.5155854,  0.8129665,  0.0871897, 0.2348464,  0.1846561,  0.8091402,  0.9271948,  0.2960011, -0.013189 ,  0.5233978,  0.8092403,  0.0725451, -0.2037076, 0.1924306,  0.8196916])
            mean_dir_vec = mean_dir_vec.reshape(-1, 3)
            mean_pose = np.array([ 0.0000306,  0.0004946,  0.0008437,  0.0033759, -0.2051629, -0.0143453,  0.0031566, -0.3054764,  0.0411491,  0.0029072, -0.4254303, -0.001311 , -0.1458413, -0.1505532, -0.0138192, -0.2835603,  0.0670333,  0.0107002, -0.2280813,  0.112117 , 0.2087789,  0.1523502, -0.1521499, -0.0161503,  0.291909 , 0.0644232,  0.0040145,  0.2452035,  0.1115339,  0.2051307])
            mean_pose = mean_pose.reshape(-1, 3)
            new_mean_pose = mean_pose @ p
            new_mean__dir_vec = mean_dir_vec @ p
            if self.save_delta_pos:
                self.delta_pose = np.zeros((self.new_out_dir_vec.shape[0], 2, 8))  # nb frames, desired/real value, 8 joints
        elif self.gesture_style == 'velocity_test':
            if self.save_delta_pos:
                self.local_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/velocity/'
                if not os.path.exists(self.local_path):
                    os.makedirs(self.local_path)
                delta_pose_len = 60 * int(self.rate)
                self.delta_pose = np.zeros((delta_pose_len, 2, 8))  # nb frames, desired/real value, 8 joints
        elif self.gesture_style == 'acceleration_test':
            if self.save_delta_pos:
                self.local_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/acceleration/'
                if not os.path.exists(self.local_path):
                    os.makedirs(self.local_path)
                delta_pose_len = 60 * int(self.rate)
                self.delta_pose = np.zeros((delta_pose_len, 2, 8))  # nb frames, desired/real value, 8 joints
        elif self.gesture_style == 'test_dataset':
            self.local_path = '/local_scratch2/aauterna/output/generation_results/'
            self.save_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/test_dataset/'
            self.seq_id = 0
            self.p = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]])
            self.mean_dir_vec = np.array([ 0.0154009, -0.9690125, -0.0884354, -0.0022264, -0.8655276, 0.4342174, -0.0035145, -0.8755367, -0.4121039, -0.9236511, 0.3061306, -0.0012415, -0.5155854,  0.8129665,  0.0871897, 0.2348464,  0.1846561,  0.8091402,  0.9271948,  0.2960011, -0.013189 ,  0.5233978,  0.8092403,  0.0725451, -0.2037076, 0.1924306,  0.8196916])
            for file in os.listdir(self.local_path):
                if file.endswith(".npy"):
                    video_name_find = '[^a-z]+'
                    match = re.findall(video_name_find, file)
                    if match:
                        if '512' in match[0] and 'test_set':
                            self.out_dir_vec = np.load(self.local_path + file)
                            if self.n_sequences > self.out_dir_vec.shape[0]:
                                self.n_sequences = self.out_dir_vec.shape[0]
                            self.out_dir_vec = self.out_dir_vec[:self.n_sequences, :, :]
                            break
            if self.save_delta_pos:
                self.delta_pose = np.zeros((self.out_dir_vec.shape[0]*self.out_dir_vec.shape[1], 2, 8))  # nb frames, desired/real value, 8 joints
        elif self.gesture_style == 'generated_test_dataset':
            self.local_path = '/local_scratch2/aauterna/output/generation_results/'
            self.save_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/generated_test_dataset/'
            self.seq_id = 0
            self.p = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]])
            for file in os.listdir(self.local_path):
                if file.endswith(".pkl"):
                    if 'generated' in file and '194' in file:
                        pickle_find = '\d{2}_\d{2}_\d{2}'
                        match = re.findall(pickle_find, file)
                        if match:
                            self.ckpt_date = re.findall('experiment_[0-9]+', file)[0]  # match[0]
                            data = pickle.load(open('{}{}'.format(self.local_path, file), 'rb'))
                            self.out_dir_vec = data['out_dir_vec']
                            self.out_dir_vec = self.out_dir_vec.reshape(-1, self.out_dir_vec.shape[2], self.out_dir_vec.shape[3])
                            if self.n_sequences > self.out_dir_vec.shape[0]:
                                self.n_sequences = self.out_dir_vec.shape[0]
                            self.out_dir_vec = self.out_dir_vec[:self.n_sequences, :, :]
                            break
            if self.save_delta_pos:
                self.delta_pose = np.zeros((self.out_dir_vec.shape[0]*self.out_dir_vec.shape[1], 2, 8))  # nb frames, desired/real value, 8 joints
        elif self.gesture_style == 'mean_pose':
            self.save_path = '/local_scratch/aauterna/Documents/Work/projects/spring/play_gestures/test/mean_pose/'
            if not os.path.exists(self.save_path):
                    os.makedirs(self.save_path)
            self.mean_dir_vec = np.array([ 0.0154009, -0.9690125, -0.0884354, -0.0022264, -0.8655276, 0.4342174, -0.0035145, -0.8755367, -0.4121039, -0.9236511, 0.3061306, -0.0012415, -0.5155854,  0.8129665,  0.0871897, 0.2348464,  0.1846561,  0.8091402,  0.9271948,  0.2960011, -0.013189 ,  0.5233978,  0.8092403,  0.0725451, -0.2037076, 0.1924306,  0.8196916])
            self.p = np.array([[0, -1, 0], [0, 0, -1], [1, 0, 0]])
            self.max_time_sec = 10
            if self.save_delta_pos:
                self.delta_pose = np.zeros((int(self.rate*self.max_time_sec), 2, 8))  # nb frames, desired/real value, 8 joints
        elif self.gesture_style == 'basic_poses':
            self.max_time_sec = 3
            self.basic_pose_idx = 0
            self.basic_pose_max = False
        else:        
            rospy.logerr("gesture style {} not implemented".format(self.gesture_style))
            rospy.signal_shutdown("gesture style {} not implemented".format(self.gesture_style))


    def step(self, event):
        if self.to_shutdown:
            return
        if self.gesture_style == 'np_data_saved':
            # if self.pose_idx == self.joints_pose_saved.shape[0]:
            #     rospy.signal_shutdown("All arm gestures have been played")
            # else:
            #     dir_vec = np.array([[0., 0., 0.],
            #                         self.dir_vec_saved[self.pose_idx][-4],
            #                         self.dir_vec_saved[self.pose_idx][-3],
            #                         self.dir_vec_saved[self.pose_idx][2],
            #                         self.dir_vec_saved[self.pose_idx][3]])
            #     print(dir_vec.shape)
            #     # import sys
            #     # sys.exit(0)
            #     joints = self.play_arm_gestures.compute_angle_pos_from_dir_vec(dir_vec=dir_vec)
            #     if self.j2_offset:
            #         joints[1] += np.radians(self.j2_offset)
            #         joints[5] += np.radians(self.j2_offset)
            if self.pose_idx == self.joints_pose_saved.shape[0]:
                rospy.signal_shutdown("All arm gestures have been played")
            else:
                joints = self.joints_pose_saved[self.pose_idx]
        if self.gesture_style == 'checkpoint':
            if self.pose_idx == 0:
                self.time_ckpt = time.time()
                # pass
                cmd = 'sshpass -p pal ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" pal@ari-10c -- "timeout {} play {}{} 2> /dev/null " &'.format(self.timeout_ckpt-2, 'ted_gen_2_vid_730', self.audio_extension)
                rospy.loginfo(cmd)
                os.system(cmd)
                time.sleep(1)
            if self.pose_idx == self.new_out_dir_vec.shape[0] or time.time() - self.time_ckpt >= self.timeout_ckpt:
                rospy.loginfo('Time duration {} seconds'.format(time.time() - self.time_ckpt))
                rospy.logerr("All arm gestures have been played")
                rospy.signal_shutdown("All arm gestures have been played")

            l_r_shoulder = self.new_out_poses[self.pose_idx][4] - self.new_out_poses[self.pose_idx][7]
            dir_vec = np.array([l_r_shoulder,
                                self.new_out_dir_vec[self.pose_idx][-2],
                                self.new_out_dir_vec[self.pose_idx][-1],
                                self.new_out_dir_vec[self.pose_idx][-5],
                                self.new_out_dir_vec[self.pose_idx][-4]])
            joints = self.play_arm_gestures.compute_angle_pos_from_dir_vec(dir_vec=dir_vec)
            if self.j2_offset:
                joints[1] += np.radians(self.j2_offset)
                joints[5] += np.radians(self.j2_offset)
        elif self.gesture_style == 'velocity_test':
            joints = self.velocity_test(event=event, joint_name='arm_left_1_joint', mode='exponential', start_vel=0., stop_vel=3., num=50)
        elif self.gesture_style == 'acceleration_test':
            joints = self.acceleration_test(event=event, joint_name='arm_left_2_joint', mode='constant', start_acc=0., stop_acc=15., num=50)
        elif self.gesture_style == 'test_dataset':
            joints = self.test_dataset(event=event)
        elif self.gesture_style == 'generated_test_dataset':
            joints = self.generated_test_dataset(event=event)
        elif self.gesture_style == 'mean_pose':
            joints = self.mean_pose_test(event=event)
        elif self.gesture_style == 'basic_poses':
            joints = self.basic_poses_test(event=event)
        self.publish_arm_gestures(joints, time_from_start=0.2)
        self.compute_delta_pos(joints)
        self.pose_idx += 1


    def reset_arm_position(self, joints, reset_time=2., time_from_start=2.):
        tt = time.time()
        while time.time() - tt < reset_time:
            self.publish_arm_gestures(joints, time_from_start=time_from_start)


    def publish_arm_gestures(self, arm_gestures, time_from_start=0.1):
        lj1 , lj2, lj3, lj4 ,rj1, rj2, rj3, rj4 = arm_gestures
        # rospy.logdebug('joint values : {}'.format(arm_gestures))

        # Left arm publication
        left_arm_gestures_msg = JointTrajectory()
        left_p = JointTrajectoryPoint()
        left_p.positions = [None] * 4
        left_p.positions[0] = lj1
        left_p.positions[1] = lj2
        left_p.positions[2] = lj3
        left_p.positions[3] = lj4
        left_arm_gestures_msg.joint_names = [None]*4
        left_arm_gestures_msg.joint_names[0] = "arm_left_1_joint"
        left_arm_gestures_msg.joint_names[1] = "arm_left_2_joint"
        left_arm_gestures_msg.joint_names[2] = "arm_left_3_joint"
        left_arm_gestures_msg.joint_names[3] = "arm_left_4_joint"
        left_arm_gestures_msg.points = [None]
        left_arm_gestures_msg.points[0] = left_p
        left_arm_gestures_msg.points[0].time_from_start = rospy.Duration.from_sec(time_from_start)
        self._left_arm_pos_pub.publish(left_arm_gestures_msg)

        # Right arm publication
        right_arm_gestures_msg = JointTrajectory()
        right_p = JointTrajectoryPoint()
        right_p.positions = [None] * 4
        right_p.positions[0] = rj1
        right_p.positions[1] = rj2
        right_p.positions[2] = rj3
        right_p.positions[3] = rj4
        right_arm_gestures_msg.joint_names = [None]*4
        right_arm_gestures_msg.joint_names[0] = "arm_right_1_joint"
        right_arm_gestures_msg.joint_names[1] = "arm_right_2_joint"
        right_arm_gestures_msg.joint_names[2] = "arm_right_3_joint"
        right_arm_gestures_msg.joint_names[3] = "arm_right_4_joint"
        right_arm_gestures_msg.points = [None]
        right_arm_gestures_msg.points[0] = right_p
        right_arm_gestures_msg.points[0].time_from_start = rospy.Duration.from_sec(time_from_start)
        self._right_arm_pos_pub.publish(right_arm_gestures_msg)

    
    def compute_delta_pos(self, joint_targets):
        lj1 , lj2, lj3, lj4 ,rj1, rj2, rj3, rj4 = joint_targets
        lj1_v = self.joint_states_data.position[self.arm_left_1_joint_ind]
        lj2_v = self.joint_states_data.position[self.arm_left_2_joint_ind]
        lj3_v = self.joint_states_data.position[self.arm_left_3_joint_ind]
        lj4_v = self.joint_states_data.position[self.arm_left_4_joint_ind]
        rj1_v = self.joint_states_data.position[self.arm_right_1_joint_ind]
        rj2_v = self.joint_states_data.position[self.arm_right_2_joint_ind]
        rj3_v = self.joint_states_data.position[self.arm_right_3_joint_ind]
        rj4_v = self.joint_states_data.position[self.arm_right_4_joint_ind]
        delta = np.array([[lj1 - lj1_v,
                           lj2 - lj2_v,
                           lj3 - lj3_v,
                           lj4 - lj4_v],
                          [rj1 - rj1_v,
                           rj2 - rj2_v,
                           rj3 - rj3_v,
                           rj4 - rj4_v]])
        delta = abs(delta)
        # rospy.loginfo('delta : {}, mean average error : {}'.format(delta, np.mean(delta)))
        # rospy.loginfo('mean error : {} (left/right arm)'.format(np.mean(delta, axis=1)))
        rospy.loginfo('joints desired : {}'.format(joint_targets))
        if self.save_delta_pos:
            self.delta_pose[self.pose_idx, :, :] = np.array([[lj1 , lj2, lj3, lj4 ,rj1, rj2, rj3, rj4],
                                                             [lj1_v , lj2_v, lj3_v, lj4_v ,rj1_v, rj2_v, rj3_v, rj4_v]])
        return delta


    def basic_poses_test(self, event):
        if self.pose_idx == 0:
            self.reset_arm_position(joints=[0.]*self.play_arm_gestures.n_joints, reset_time=2., time_from_start=0.15)
            self.start_time = time.time()
        dir_vec = np.zeros((self.play_arm_gestures.n_dir_vec, 3))
        if self.basic_pose_idx == 0:
            if time.time() - self.start_time < self.max_time_sec:
                dir_vec[1] = [0, 0, -1]
                dir_vec[2] = [1, 0, 0]
                dir_vec[3] = [0, 0, -1]
                dir_vec[4] = [0, -1, 0]
            else:
                self.start_time = time.time()
                self.basic_pose_idx += 1
        if self.basic_pose_idx == 1:
            if time.time() - self.start_time < self.max_time_sec:
                dir_vec[1] = [1, 0, 0]
                dir_vec[2] = [0, 0, 1]
                dir_vec[3] = [0, -1, 0]
                dir_vec[4] = [0, 0, 1]
            else:
                self.start_time = time.time()
                self.basic_pose_idx += 1
        if self.basic_pose_idx == 2:
            if time.time() - self.start_time < self.max_time_sec:
                dir_vec[1] = [1, 1, -1]
                dir_vec[2] = [1, 0, 0]
                dir_vec[3] = [1, -1, 1]
                dir_vec[4] = [0, 0, 1]
            else:
                self.start_time = time.time()
                self.basic_pose_idx += 1
        if self.basic_pose_idx == 3:
            if time.time() - self.start_time < self.max_time_sec:
                dir_vec[1] = [1, 1, -1]
                dir_vec[2] = [1, 1, 1]
                dir_vec[3] = [1, -1, 1]
                dir_vec[4] = [1, 1, 1]
            else:
                self.start_time = time.time()
                self.basic_pose_max = True
        if self.basic_pose_max:
            time.sleep(1)
            rospy.logerr("All the {} basic poses were tested".format(self.basic_pose_idx))
            rospy.signal_shutdown("All the {} basic poses were tested".format(self.basic_pose_idx))
        
        joints = self.play_arm_gestures.compute_angle_pos_from_dir_vec(dir_vec=dir_vec)        
        joints = np.round(joints, decimals=5)
        limit, joints = self.play_arm_gestures.check_joint_limits(joints=joints)
        if limit:           
                # time.sleep(1)
                rospy.logerr("Joint limit reached in one or more joint")
                # rospy.signal_shutdown("Joint limit reached in one or more joint")
        return joints


    def mean_pose_test(self, event):
        if self.pose_idx == 0:
            self.reset_arm_position(joints=[0.]*self.play_arm_gestures.n_joints, reset_time=2., time_from_start=0.1)
            self.start_time = time.time()
        if time.time() - self.start_time >= self.max_time_sec:
            time.sleep(1)
            rospy.logerr("Max time reached : {} seconds".format(self.max_time_sec))
            rospy.signal_shutdown("Max time reached : {} seconds".format(self.max_time_sec))
        out_dir_vec = self.mean_dir_vec.reshape(9, 3)
        out_dir_vec[:, 0] = -out_dir_vec[:, 0]
        out_dir_vec = out_dir_vec @ self.p
        dir_vec = np.array([[0., 0., 0.], 
                            out_dir_vec[-2],
                            out_dir_vec[-1],
                            out_dir_vec[-5],
                            out_dir_vec[-4]])
        joints = self.play_arm_gestures.compute_angle_pos_from_dir_vec(dir_vec=dir_vec)        
        joints = np.round(joints, decimals=5)
        limit, joints = self.play_arm_gestures.check_joint_limits(joints=joints)
        if limit:           
                # time.sleep(1)
                rospy.logerr("Joint limit reached in one or more joint")
                # rospy.signal_shutdown("Joint limit reached in one or more joint")
        return joints


    def generated_test_dataset(self, event):
        if self.pose_idx > 0 and self.pose_idx % self.out_dir_vec.shape[1] == 0:
            rospy.logdebug("Sequence id {}/{} was played".format(self.seq_id+1, self.out_dir_vec.shape[0]))
            self.seq_id += 1
        if self.seq_id == self.out_dir_vec.shape[0]:
            time.sleep(1)
            rospy.logerr("All the ({}/{}) sequences of the generated test dataset were played".format(self.out_dir_vec.shape[0], self.out_dir_vec.shape[0]))
            rospy.signal_shutdown("All the sequences of the generated test dataset of length {}/{} were played".format(self.out_dir_vec.shape[0], self.out_dir_vec.shape[0]))
        out_dir_vec = self.out_dir_vec[self.seq_id, self.pose_idx % self.out_dir_vec.shape[1], :]
        out_dir_vec = out_dir_vec.reshape(9, 3)
        out_dir_vec[:, 0] = -out_dir_vec[:, 0]
        out_dir_vec = out_dir_vec @ self.p
        dir_vec = np.array([[0., 0., 0.], 
                            out_dir_vec[-2],
                            out_dir_vec[-1],
                            out_dir_vec[-5],
                            out_dir_vec[-4]])
        joints = self.play_arm_gestures.compute_angle_pos_from_dir_vec(dir_vec=dir_vec)
        if self.j2_offset:
                joints[1] += np.radians(self.j2_offset)
                joints[5] += np.radians(self.j2_offset)
        if self.pose_idx % self.out_dir_vec.shape[1] == 0:
            rospy.logdebug("Sequence id {}/{} will be played".format(self.seq_id+1, self.out_dir_vec.shape[0]))
            self.reset_arm_position(joints=joints, reset_time=2., time_from_start=0.1)
        joints = np.round(joints, decimals=5)
        limit, joints = self.play_arm_gestures.check_joint_limits(joints=joints)
        if limit:           
                # time.sleep(1)
                rospy.logerr("Joint limit reached in one or more joint")
                # rospy.signal_shutdown("Joint limit reached in one or more joint")
        return joints


    def test_dataset(self, event):
        if self.pose_idx > 0 and self.pose_idx % self.out_dir_vec.shape[1] == 0:
            rospy.logdebug("Sequence id {}/{} was played".format(self.seq_id+1, self.out_dir_vec.shape[0]))
            self.seq_id += 1
        if self.seq_id == self.out_dir_vec.shape[0]:
            time.sleep(1)
            rospy.logerr("All the ({}/{}) sequences of the test dataset were played".format(self.out_dir_vec.shape[0], self.out_dir_vec.shape[0]))
            rospy.signal_shutdown("All the sequences of the test dataset of length {}/{} were played".format(self.out_dir_vec.shape[0], self.out_dir_vec.shape[0]))
        out_dir_vec = self.out_dir_vec[self.seq_id, self.pose_idx % self.out_dir_vec.shape[1], :] + self.mean_dir_vec
        out_dir_vec = out_dir_vec.reshape(9, 3)
        out_dir_vec[:, 0] = -out_dir_vec[:, 0]
        out_dir_vec = out_dir_vec @ self.p
        dir_vec = np.array([[0., 0., 0.], 
                            out_dir_vec[-2],
                            out_dir_vec[-1],
                            out_dir_vec[-5],
                            out_dir_vec[-4]])
        joints = self.play_arm_gestures.compute_angle_pos_from_dir_vec(dir_vec=dir_vec)
        if self.j2_offset:
                joints[1] += np.radians(self.j2_offset)
                joints[5] += np.radians(self.j2_offset)
        if self.pose_idx % self.out_dir_vec.shape[1] == 0:
            rospy.logdebug("Sequence id {}/{} will be played".format(self.seq_id+1, self.out_dir_vec.shape[0]))
            self.reset_arm_position(joints=joints, reset_time=2., time_from_start=0.1)
        joints = np.round(joints, decimals=5)
        limit, joints = self.play_arm_gestures.check_joint_limits(joints=joints)
        if limit:           
                # time.sleep(1)
                rospy.logerr("Joint limit reached in one or more joint")
                # rospy.signal_shutdown("Joint limit reached in one or more joint")
        return joints


    def set_value_test(self, joint_name, start=0., stop=1., num=50, mode='constant'):
        if self.gesture_style == 'velocity_test':
            _, start = self.play_arm_gestures.check_velocity_limits(veloctiy=start, joint_name=joint_name)
            _, stop = self.play_arm_gestures.check_velocity_limits(veloctiy=stop, joint_name=joint_name)
        if self.gesture_style == 'acceleration_test':
            _, start = self.play_arm_gestures.check_acceleration_limits(acceleration=start, joint_name=joint_name)
            _, stop = self.play_arm_gestures.check_acceleration_limits(acceleration=stop, joint_name=joint_name)
        if self.pose_idx == 0:
            self.linspace_idx = 0
            if mode == 'constant':
                self.linspace_val = np.linspace(start=stop, stop=stop, num=num)
            elif mode == 'linear':
                self.linspace_val = np.linspace(start=start, stop=stop, num=num)
            elif mode == 'exponential':
                self.linspace_val = exponential_space(start=start, stop=stop, num=num)
            elif mode == 'log':
                self.linspace_val = log_space(start=start, stop=stop, num=num)
            elif mode == 'power':
                self.linspace_val = power_space(start=start, stop=stop, power=2, num=num)
            else:
                rospy.logwarn('{} not implemented, falling back on constant mode'.format(mode))
                self.linspace_val = np.linspace(start=stop, stop=stop, num=num)
        
        if self.linspace_idx >= self.linspace_val.shape[0]:
            value = self.linspace_val[-1]
        else:
            value = self.linspace_val[self.linspace_idx]
        self.linspace_idx += 1
        return value


    def velocity_test(self, event, joint_name, start_vel=1.0, stop_vel=1., num=50, mode='constant'):
        limit = False
        velocity = self.set_value_test(joint_name=joint_name, start=start_vel, stop=stop_vel, num=num, mode=mode)
        rospy.loginfo('velocity desired : {}'.format(velocity))
        if self.pose_idx == 0:
            if self.save_delta_pos:
                self.checkpoint_name = joint_name + '_velocity_test_{0}_{1}_{2}_{3}'.format(start_vel, stop_vel, num, mode)
            lj1 = 0.
            lj2 = np.pi/2
            lj3 = 0.
            lj4 = np.pi/2
            rj1 = 0.
            rj2 = 0.
            rj3 = 0.
            rj4 = np.pi/2
            self.reset_arm_position(joints=[lj1, lj2, lj3, lj4, rj1, rj2, rj3, rj4], reset_time=2., time_from_start=0.1)
        else:
            lj1 = self.joint_states_data.position[self.arm_left_1_joint_ind]
            lj2 = self.joint_states_data.position[self.arm_left_2_joint_ind]
            lj3 = self.joint_states_data.position[self.arm_left_3_joint_ind]
            lj4 = self.joint_states_data.position[self.arm_left_4_joint_ind]
            rj1 = self.joint_states_data.position[self.arm_right_1_joint_ind]
            rj2 = self.joint_states_data.position[self.arm_right_2_joint_ind]
            rj3 = self.joint_states_data.position[self.arm_right_3_joint_ind]
            rj4 = self.joint_states_data.position[self.arm_right_4_joint_ind]
            if joint_name == 'arm_right_1_joint' or joint_name == 'arm_left_1_joint':
                rj1 += np.sign(self.sign) * 1/self.rate * velocity
                lj1 += np.sign(self.sign) * 1/self.rate * velocity
            if joint_name == 'arm_right_2_joint' or joint_name == 'arm_left_2_joint':
                rj2 += np.sign(self.sign) * 1/self.rate * velocity
                lj2 += np.sign(self.sign) * 1/self.rate * velocity
            if joint_name == 'arm_right_3_joint' or joint_name == 'arm_left_3_joint':
                rj3 += np.sign(self.sign) * 1/self.rate * velocity
                lj3 += np.sign(self.sign) * 1/self.rate * velocity
            if joint_name == 'arm_right_4_joint' or joint_name == 'arm_left_4_joint':
                rj4 += np.sign(self.sign) * 1/self.rate * velocity
                lj4 += np.sign(self.sign) * 1/self.rate * velocity

        joints = np.round([lj1, lj2, lj3, lj4, rj1, rj2, rj3, rj4], decimals=5)
        limit, joints = self.play_arm_gestures.check_joint_limits(joints=joints)
        if limit:
            if self.sign == 1:
                self.sign = -1
                self.linspace_idx = 0
            elif self.sign == -1:
                time.sleep(1)
                rospy.logerr("Joint limit reached in one or more joint")
                rospy.signal_shutdown("Joint limit reached in one or more joint")
        return joints


    def acceleration_test(self, event, joint_name, start_acc=1.0, stop_acc=1., num=50, mode='constant'):
        limit = False
        acceleration = self.set_value_test(joint_name=joint_name, start=start_acc, stop=stop_acc, num=num, mode=mode)
        rospy.loginfo('acceleration desired : {}'.format(acceleration))
        if self.pose_idx == 0:
            if self.save_delta_pos:
                self.checkpoint_name = joint_name + '_acceleration_test_{0}_{1}_{2}_{3}'.format(start_acc, stop_acc, num, mode)
            self.lj1 = 0.
            self.lj2 = 0.
            self.lj3 = 0.
            self.lj4 = 0.
            self.rj1 = 0.
            self.rj2 = 0.
            self.rj3 = 0.
            self.rj4 = 0.
            lj1, rj1, lj2, rj2, lj3, rj3, lj4, rj4 = self.lj1, self.rj1, self.lj2, self.rj2, self.lj3, self.rj3, self.lj4, self.rj4
            self.reset_arm_position(joints=[lj1 , lj2, lj3, lj4 ,rj1, rj2, rj3, rj4], reset_time=2., time_from_start=0.1)
        else:
            lj1_t = self.joint_states_data.position[self.arm_left_1_joint_ind]
            lj2_t = self.joint_states_data.position[self.arm_left_2_joint_ind]
            lj3_t = self.joint_states_data.position[self.arm_left_3_joint_ind]
            lj4_t = self.joint_states_data.position[self.arm_left_4_joint_ind]
            rj1_t = self.joint_states_data.position[self.arm_right_1_joint_ind]
            rj2_t = self.joint_states_data.position[self.arm_right_2_joint_ind]
            rj3_t = self.joint_states_data.position[self.arm_right_3_joint_ind]
            rj4_t = self.joint_states_data.position[self.arm_right_4_joint_ind]
            lj1, rj1, lj2, rj2, lj3, rj3, lj4, rj4 = lj1_t, rj1_t, lj2_t, rj2_t, lj3_t, rj3_t, lj4_t, rj4_t
            if joint_name == 'arm_right_1_joint' or joint_name == 'arm_left_1_joint':
                rj1 = np.sign(self.sign) * 1/self.rate**2 * acceleration + 2 * rj1_t - self.rj1
                lj1 = np.sign(self.sign) * 1/self.rate**2 * acceleration + 2 * lj1_t - self.lj1
                self.rj1 = rj1_t
                self.lj1 = lj1_t
            if joint_name == 'arm_right_2_joint' or joint_name == 'arm_left_2_joint':
                rj2 = np.sign(self.sign) * 1/self.rate**2 * acceleration + 2 * rj2_t - self.rj2
                lj2 = np.sign(self.sign) * 1/self.rate**2 * acceleration + 2 * lj2_t - self.lj2
                self.rj2 = rj2_t
                self.lj2 = lj2_t
            if joint_name == 'arm_right_3_joint' or joint_name == 'arm_left_3_joint':
                rj3 = np.sign(self.sign) * 1/self.rate**2 * acceleration + 2 * rj3_t - self.rj3
                lj3 = np.sign(self.sign) * 1/self.rate**2 * acceleration + 2 * lj3_t - self.lj3
                self.rj3 = rj3_t
                self.lj3 = lj3_t
            if joint_name == 'arm_right_4_joint' or joint_name == 'arm_left_4_joint':
                rj4 = np.sign(self.sign) * 1/self.rate**2 * acceleration + 2 * rj4_t - self.rj4
                lj4 = np.sign(self.sign) * 1/self.rate**2 * acceleration + 2 * lj4_t - self.lj4
                self.rj4 = rj4_t
                self.lj4 = lj4_t

        joints = np.round([lj1, lj2, lj3, lj4, rj1, rj2, rj3, rj4], decimals=5)
        limit, joints = self.play_arm_gestures.check_joint_limits(joints=joints)
        if limit:
            if self.sign == 1:
                self.sign = -1
                self.linspace_idx = 0
                self.lj1, self.rj1, self.lj2, self.rj2, self.lj3, self.rj3, self.lj4, self.rj4 = lj1, rj1, lj2, rj2, lj3, rj3, lj4, rj4
            elif self.sign == -1:
                time.sleep(1)
                rospy.logerr("Joint limit reached in one or more joint")
                rospy.signal_shutdown("Joint limit reached in one or more joint")
        return joints


    def shutdown(self):
        rospy.loginfo("Stopping the {} node".format(self.node_name))
        self.to_shutdown = True

        if self.gesture_style == 'checkpoint':
            cmd = 'sshpass -p pal ssh -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" pal@ari-10c -- "killall play"'
            rospy.loginfo(cmd)
            os.system(cmd)

        # Close evething
        self.reset_arm_position(joints=[0.]*self.play_arm_gestures.n_joints, reset_time=2., time_from_start=2.)

        # Saving data
        if self.save_delta_pos:
            dt_string = datetime.now().strftime("%d_%m_%Y_%H_%M_%S")
            np_data = self.delta_pose
            if self.gesture_style == 'test_dataset':
                filename_np = 'len_{}_date_{}'.format(self.out_dir_vec.shape[0], dt_string)
                if self.simulation:
                    filename_np += '_sim'
                if self.safe_command:
                    filename_np += '_safe_cmd'
                if self.j2_offset != 0.:
                    if self.j2_offset < 0.:
                        filename_np += '_j2_offset_m{}'.format(np.abs(self.j2_offset))
                    if self.j2_offset > 0.:
                        filename_np += '_j2_offset_{}'.format(self.j2_offset)
                path_np = '{}{}.npy'.format(self.save_path, filename_np)
            elif self.gesture_style == 'generated_test_dataset':
                filename_np = 'ckpt_{}_len_{}_date_{}'.format(self.ckpt_date, self.out_dir_vec.shape[0], dt_string)
                if self.simulation:
                    filename_np += '_sim'
                if self.safe_command:
                    filename_np += '_safe_cmd'
                if self.j2_offset != 0.:
                    if self.j2_offset < 0.:
                        filename_np += '_j2_offset_m{}'.format(np.abs(self.j2_offset))
                    if self.j2_offset > 0.:
                        filename_np += '_j2_offset_{}'.format(self.j2_offset)
                path_np = '{}{}.npy'.format(self.save_path, filename_np)
            elif self.gesture_style == 'mean_pose':
                filename_np = 'mean_pose_len_{}_date_{}'.format(self.max_time_sec, dt_string)
                if self.simulation:
                    filename_np += '_sim'
                if self.safe_command:
                    filename_np += '_safe_cmd'
                path_np = '{}{}.npy'.format(self.save_path, filename_np)
            else:
                filename_np = '{}_delta_pose_{}.npy'.format(self.checkpoint_name, dt_string)
                if self.simulation:
                    filename_np += '_sim'
                if self.safe_command:
                    filename_np += '_safe_cmd'
                path_np = '{}{}.npy'.format(self.local_path, filename_np)
                np_data = self.delta_pose[:self.pose_idx - 2]
            np.save(open(path_np, 'wb'), np_data)

        rospy.loginfo("Killing the {} node".format(self.node_name))


    def _joint_states_callback(self, data):
        self.joint_states_data = data


    def _check_all_sensors_ready(self):
        rospy.logdebug("START ALL SENSORS READY")
        self._check_joint_states_ready()
        rospy.logdebug("ALL SENSORS READY")


    def _check_joint_states_ready(self):
        self.joint_states_data = None
        rospy.logdebug("Waiting for {} to be READY...".format(self.joint_states_topic))
        while self.joint_states_data is None and not rospy.is_shutdown():
            try:
                self.joint_states_data = rospy.wait_for_message(self.joint_states_topic, JointState, timeout=5.0)
                rospy.logdebug("Current {} READY=>".format(self.joint_states_topic))

            except:
                rospy.logerr("Current {} not ready yet, retrying for getting joint states".format(self.joint_states_topic))
        if not self.get_indexes:
            for index, name in enumerate(self.joint_states_data.name):
                if name == "arm_left_1_joint":
                    self.arm_left_1_joint_ind = index
                if name == "arm_left_2_joint":
                    self.arm_left_2_joint_ind = index
                if name == "arm_left_3_joint":
                    self.arm_left_3_joint_ind = index
                if name == "arm_left_4_joint":
                    self.arm_left_4_joint_ind = index
                if name == "arm_right_1_joint":
                    self.arm_right_1_joint_ind = index
                if name == "arm_right_2_joint":
                    self.arm_right_2_joint_ind = index
                if name == "arm_right_3_joint":
                    self.arm_right_3_joint_ind = index
                if name == "arm_right_4_joint":
                    self.arm_right_4_joint_ind = index
            self.get_indexes = True
        return self.joint_states_data
