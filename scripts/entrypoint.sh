#!/bin/bash
set -e

# setup ros environment
echo $@
source /opt/ros/noetic/setup.bash
source /home/ros/robot_behavior_ws/devel/setup.bash
exec $@
