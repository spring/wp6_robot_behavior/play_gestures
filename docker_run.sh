#!/usr/bin/env bash
# See http://wiki.ros.org/docker/Tutorials/Hardware%20Acceleration

set -euo pipefail

# See README.md for building this image.
CONTAINER_NAME=gestures_generation_test
DOCKER_IMAGE=registry.gitlab.inria.fr/spring/dockers/gestures_generation:0.0.5

# Which GPUs to use; see https://github.com/NVIDIA/nvidia-docker
GPUS="all"

XSOCK=/tmp/.X11-unix

XAUTH=/tmp/docker.xauth
XAUTH_DOCKER=/tmp/.docker.xauth

if [ ! -f $XAUTH ]
then
    xauth_list=$(xauth nlist :0 | sed -e 's/^..../ffff/')
    if [ ! -z "$xauth_list" ]
    then
        echo "$xauth_list" | xauth -f $XAUTH nmerge -
    else
        touch $XAUTH
    fi
    chmod a+r $XAUTH
fi

docker run --rm -it \
    --name "$CONTAINER_NAME" \
    --add-host ari-10c:192.168.0.10 \
    --env="ROS_IP=192.168.0.1" \
    --env="ROS_MASTER_URI=http://ari-10c:11311" \
    --network=host \
    --privileged \
    --gpus "$GPUS" \
    --env="DISPLAY" \
    --env="QT_X11_NO_MITSHM=1" \
    --env="XAUTHORITY=$XAUTH_DOCKER" \
    --volume="$XSOCK:$XSOCK:rw" \
    --volume="$XAUTH:$XAUTH_DOCKER:rw" \
    $DOCKER_IMAGE \
    bash
