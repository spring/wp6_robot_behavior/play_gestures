#!/bin/bash

# both do the same, but one might sometimes fail
git submodule update --init --recursive --remote
git pull --recurse-submodules