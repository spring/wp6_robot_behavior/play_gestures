<a name="readme-top"></a>

<!-- PROJECT SHIELDS -->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url] -->

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <!-- <a href="https://github.com/github_username/repo_name">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a> -->

<h3 align="center">ROS Play Gestures</h3>

<p align="center">
  This project is the ROS implementation of the <a href="https://gitlab.inria.fr/robotlearn/gestures_generation"><strong>Gesture Generation from Trimodal Context</strong></a> repository based on the <a href="https://arxiv.org/pdf/2009.02119.pdf"><strong>Speech Gesture Generation from the Trimodal Context of Text, Audio, and Speaker Identity</strong></a> paper. This implementation is working in real time on the <a href="https://pal-robotics.com/robots/ari/"><strong>ARI</strong></a> robot from <a href="https://pal-robotics.com/"><strong>Pal Robotics</strong></a>. The main inputs are the audio inputs from different Text to Speech (TTS) algorithm and the text inputs from the user. See the <a href="#usage">usage</a> section to understand how it is working.
</p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>    
    <li><a href="#usage">Usage</a>
      <ul>
        <li><a href="#gestures-action-server">Gestures Action Server</a></li>
        <li><a href="#gestures-action-client">Gestures Action Client</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- GETTING STARTED -->
## Getting Started

<details>
<summary>Getting Started</summary>



### Prerequisites

In order to run this ROS implementation, you can install the different dependencies manually or directly build the docker container image. On your local machine, you will need to install [ROS](http://wiki.ros.org/ROS/Installation) (if you want to install manually) or [Docker](https://docs.docker.com/engine/install/) (if you just want to build the docker container image).

First, install the [Play Gestures](https://gitlab.inria.fr/spring/wp6_robot_behavior/play_gestures) repository and its submodules:

```sh
git clone https://gitlab.inria.fr/spring/wp6_robot_behavior/play_gestures
git checkout main
git pull
git submodule update --init
git submodule update --recursive --remote
cd modules/Gesture-Generation-from-Trimodal-Context/
git checkout master
git pull
cd ../../
```
You need to create a `model` folder in the `src/play_gestures/config/` folder. If this folder create two other folders:
* `trained_models`: this folder contains all the `.bin` file of the saved model weights. See the [train.py](https://gitlab.inria.fr/robotlearn/gestures_generation/-/blob/master/gestures_generation/train.py?ref_type=heads) file in the [Gesture-Generation-from-Trimodal-Context](https://gitlab.inria.fr/spring/wp6_robot_behavior/play_gestures) repository to understand where these bin files are saved and what they contain.
* `sentence_encoder_models`: this folder contains two other folders, `hugging_face_models` and `tensorflow_hub_models`. See the Usage section of [README.md](https://gitlab.inria.fr/robotlearn/gestures_generation/-/blob/master/README.md?ref_type=heads) of the [Gesture-Generation-from-Trimodal-Context](https://gitlab.inria.fr/spring/wp6_robot_behavior/play_gestures) repository to know what to put in these two subfolders.

### Installation
* Manual Installation:
  ```sh
  cd modules/Gesture-Generation-from-Trimodal-Context/
  pip3 install -r requirements.txt
  pip3 install -e .
  cd ..
  git clone https://gitlab.inria.fr/creinke/exputils.git
  cd exputils/
  git checkout master
  git pull
  pip3 install -e .
  cd ../../src/
  git clone https://github.com/pal-robotics-forks/humanoid_msgs.git
  git clone https://github.com/pal-robotics/pal_msgs.git
  cd ../
  pip3 install -r requirements.txt
  source /opt/ros/noetic/setup.bash && catkin_make
  source devel/setup.bash
  ```
  A build and devel folder should have been created in your ROS workspace. See [ROS workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace) tutorial for more informations.

* Docker:
  ```sh
  export DOCKER_BUILDKIT=1
  docker build -t gestures_generation --target behavior-generator .
  ```
  A docker container image, named `gestures_generation` should have been created with the tag: `latest`.

  You can run the docker container based on this images running the [docker_run.sh](https://gitlab.inria.fr/spring/wp6_robot_behavior/play_gestures/-/blob/main/docker_run.sh?ref_type=heads) file:
  ```sh
  ./docker_run.sh
  ```
  In this bash file, you will need to change the `DOCKER_IMAGE` value with docker image name you just put before, the `ROS_IP` value, the `ROS_MASTER_URI` value.

</details>

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- USAGE EXAMPLES -->
## Usage

<details>
<summary>Usage</summary>

The Play Gestures project is an [actionlib](http://wiki.ros.org/actionlib) based package, i.e. there is an action server node to display the gestures on the robot and an action client node to start the gestures display.

### Gestures Action Server
The action server node will subscribe to the `/joint_states` topic of the robot to get the current value of each robot joint. It will publish ih the following topics:
* `/head_controller/command`: to control the head
* `/arm_left_controller/safe_command` or `/arm_left_controller/command`: to control the joints of the left side in safe manner or not
* `/arm_right_controller/safe_command` or `/arm_right_controller/command`: to control the joints of the right side in safe manner or not

To launch the gestures generation action server, run the following command:
```sh
roslaunch play_gestures play_arm_gestures_action_server.launch
```
The following parameter can be set:
* `rate` (int: 15): the publish rate of the generated gestures
* `save_delta_pos` (bool: False): to save the published and executed joint positions
* `safe_command` (bool: False): to publish the arm joint positions in the safe topic
* `j2_offset` (int: -10): offset value of the shoulder joints. ARI Robot has a very long forearms in comparison with the arms, so the visual appearance with the original shoulder positions computed by the Gesture [Generation from Trimodal Context](https://gitlab.inria.fr/robotlearn/gestures_generation) algorithm looks unnatural, an offset have been added to reduce strange behavior
* `saved_model_idx` (str: 617_1): id of the saved model that will be usd to generate the gestures
* `speaker_index` (int: -1): index of the speaker that will be used to generate the gestures. If -1, a random index will be selected, among those that have been used in the training dataset
* `decimal_num_subdivision_threshold` (float: 0.25): minimum threshold to start a 2 seconds generations, value between 0 and 1. If decimal_num_subdivision_threshold = 0.25, that means when we cut our audio sample in 2 seconds chunks (because our learned algorithm is trained on 2 seconds samples and trained to generate 2 seconds of gestures), if the last chunk is > 0.25*2 = 0.5 seconds, we will generate the last chunk as a 2 seconds sample, otherwise we will not generate anything
* `mode` (str: dialogue): mode should be chosen between `dialogue` and `monologue`. In the `monologue` mode, the robot will always alternate between displaying gestures and doing nothing. In the `dialogue` mode, the robot will alternate between displaying gestures and listining. In the later, the robot will do some random head and arm motions to show that is listining to somebody
* `tts_name` (str: palTTS): Text To Speech algorithm used to generate the speech audio. It should be chosen between:
  * `palTTS`: the TTS used by the PAL robots which is based on [Acapela](https://www.acapela-group.com/) and already built on the robot
  * `gTTS`: the TTS based on [Google Text To Speech](https://gtts.readthedocs.io/en/latest/) request. This TTS needs to have access to internet
  * `coquiTTS`: this is an open source deep learning based TTS, see [Coqui.ai](https://github.com/coqui-ai/TTS)

  The two last TTS algorithms need to be install in your project

### Gestures Action Client
To start the [Gestures Action Server](#gestures-action-server), you can launch a dedicated roslaunch file or publish directly in the goal topic of the action server.
  * roslaunch file:
    ```sh
    roslaunch play_gestures play_arm_gestures_action_client.launch language:=en_GB text:='Victor is my new friend, he is a very funny'
    ```
    Two parameters can be modified:
      * `language` (str: en_GB): language used from the TTS, between `en_GB` for English and `fr_FR` for French
      * `text` (str): the corresponding text of the gestures you want to generate
  * goal topic:
    ```sh
    rostopic pub -1 /play_arm_gestures_action/goal pal_interaction_msgs/TtsActionGoal "header:
      seq: 0
      stamp:
        secs: 0
        nsecs: 0
      frame_id: ''
    goal_id:
      stamp:
        secs: 0
        nsecs: 0
      id: ''
    goal:
      text:
        section: ''
        key: ''
        lang_id: ''
        arguments:
        - section: ''
          key: ''
          expanded: ''
      rawtext:
        text: 'Hello from the other side'
        lang_id: 'en_GB'
      speakerName: ''
      wait_before_speaking: 0.0"
    ```
    The only two lines to modify is `text` and `lang_id` in the `rawtext` section. The setting is the same as the `text` and `language` in the roslaunch file section just above.

Below, we can see two resulting examples of the Gestures Generation running in real time in ARI robot through ROS:
<div align="center">

![](documentation/test_gestures.mp4)

![](documentation/joe_biden_speech_extract.mp4)

</div>

</details>

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- ROADMAP -->
## Roadmap

- [ ] Feature 1
- [ ] Feature 2
- [ ] Feature 3
    - [ ] Nested Feature

See the [open issues](https://gitlab.inria.fr/robotlearn/gestures_generation/-/issues) for a full list of proposed features (and known issues).

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- CONTACT -->
## Contact

[Alex Auternaud](https://www.linkedin.com/public-profile/settings?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_self_edit_contact-info%3BiBJRsSXiRBaX2%2B4fc1LSWA%3D%3D) - alex.auternaud@inria.fr - alex.auternaud07@gmail.com

Project Link: [https://gitlab.inria.fr/robotlearn/gestures_generation](https://gitlab.inria.fr/robotlearn/gestures_generation)

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* []()
* []()
* []()

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/github_username/repo_name.svg?style=for-the-badge
[contributors-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/graphs/master?ref_type=heads
[forks-shield]: https://img.shields.io/github/forks/github_username/repo_name.svg?style=for-the-badge
[forks-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/forks
[stars-shield]: https://img.shields.io/github/stars/github_username/repo_name.svg?style=for-the-badge
[stars-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/starrers
[issues-shield]: https://img.shields.io/github/issues/github_username/repo_name.svg?style=for-the-badge
[issues-url]: https://gitlab.inria.fr/robotlearn/gestures_generation/-/issues
[license-shield]: https://img.shields.io/github/license/github_username/repo_name.svg?style=for-the-badge
[license-url]: https://github.com/github_username/repo_name/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/linkedin_username