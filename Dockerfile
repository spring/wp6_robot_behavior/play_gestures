FROM nvidia/cuda:11.7.1-cudnn8-devel-ubuntu20.04 as base-ros-gpu
ARG PYTHON_VERSION=3.10
SHELL ["/bin/bash", "-c"]
WORKDIR /
RUN echo "Setting up timezone..." && \
    echo 'Etc/UTC' > /etc/timezone && \
    ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime

# remove the default python3.8 and install python3.10
# RUN echo "Installing Python and Pytorch..." && \
#     apt-get update && \
#     apt-get install -q -y --no-install-recommends \
#         python3-dev \
#                        python3-pip \
#                        tzdata \
#                        wget vim tmux unzip git && \
#     pip3 install torch torchvision torchaudio && \
#     rm -rf /var/lib/apt/lists/* && \
#     rm -rf /workspace

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y tzdata wget vim tmux unzip git
RUN apt-get install -y software-properties-common gpg-agent && \
    add-apt-repository ppa:deadsnakes/ppa && \
    apt update -y

RUN apt-get install -y \
    curl \
    ca-certificates \
    python${PYTHON_VERSION} \
    python${PYTHON_VERSION}-dev \
    python${PYTHON_VERSION}-distutils

RUN ln -sf /usr/bin/python${PYTHON_VERSION} /usr/bin/python3
RUN ln -sf /usr/bin/python${PYTHON_VERSION} /usr/bin/python
RUN curl https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py
RUN python /tmp/get-pip.py


RUN echo "Installing ROS Noetic..." && \
apt-get update && \
apt-get install -q -y --no-install-recommends \
curl && \
echo "deb http://packages.ros.org/ros/ubuntu focal main" > \
/etc/apt/sources.list.d/ros-latest.list && \
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | \
apt-key add - && \
apt-get update && \
apt-get install -y --no-install-recommends \
ros-noetic-desktop && \
rm -rf /var/lib/apt/lists/* && \
echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc

FROM base-ros-gpu as ros-common
RUN apt-get update && apt-get install -y libeigen3-dev \
                                         python3-rospy \
                                         ros-noetic-tf \
                                         ros-noetic-cv-bridge \
                                         ros-noetic-perception \
                                         ros-noetic-image-transport-plugins \
                                         ros-noetic-audio-common-msgs \
                                         python3-tk \
                                         sshpass \
                                         libsndfile1 \
                                         bash-completion \
                                         mesa-utils \
                                         vim \
                                         build-essential gdb \
                                         cmake cmake-curses-gui \
                                         ssh \
                                         ffmpeg \
                                         nvtop \
                                         pkg-config libhdf5-103 libhdf5-dev \
                                         nvidia-cuda-toolkit
# RUN python3 -m pip install --upgrade pip
# RUN rm -rf /usr/lib/python3/dist-packages/*
RUN python3 -m pip install setuptools

FROM ros-common as ros-socialmpc
RUN mkdir -p /home/ros/play_gestures_ws
RUN git clone https://gitlab.inria.fr/creinke/exputils.git /home/ros/play_gestures_ws/modules/exputils
WORKDIR /home/ros/play_gestures_ws/modules/exputils
RUN python3 -m pip install --ignore-installed PyYAML
RUN python3 -m pip install -e .
ADD modules/Gesture-Generation-from-Trimodal-Context /home/ros/play_gestures_ws/modules/Gesture-Generation-from-Trimodal-Context
WORKDIR /home/ros/play_gestures_ws/modules/Gesture-Generation-from-Trimodal-Context
RUN python3 -m pip install -r requirements.txt
RUN python3 -m pip install -e .

FROM ros-socialmpc as behavior-generator
RUN groupadd -r ros && useradd -r -g ros ros
RUN chown -R ros:ros /home/ros
USER ros
ADD --chown=ros:ros src/play_gestures /home/ros/play_gestures_ws/src/play_gestures
WORKDIR /home/ros/play_gestures_ws/
ADD requirements.txt /home/ros/play_gestures_ws/
RUN python3 -m pip install -r requirements.txt
RUN git clone https://github.com/pal-robotics-forks/humanoid_msgs.git src/humanoid_msgs
RUN git clone https://github.com/pal-robotics/pal_msgs.git src/pal_msgs
WORKDIR /home/ros/play_gestures_ws/modules/exputils
RUN pip3 install -e .
WORKDIR /home/ros/play_gestures_ws/modules/Gesture-Generation-from-Trimodal-Context
RUN pip3 install -e .
WORKDIR /home/ros/play_gestures_ws/
RUN /bin/bash -c "source /opt/ros/noetic/setup.bash && catkin_make"

# To do download every time the tts weights
COPY tts/ /home/ros/.local/share/tts

COPY ./scripts/entrypoint.sh /
RUN /bin/bash -c "echo 'source /opt/ros/noetic/setup.bash' >> ~/.bashrc"
RUN /bin/bash -c "echo 'source /home/ros/play_gestures_ws/devel/setup.bash' >> ~/.bashrc"
# ENTRYPOINT ["/entrypoint.sh", "roslaunch", "robot_behavior", "main.launch"]
ENTRYPOINT [""]
CMD [""]
